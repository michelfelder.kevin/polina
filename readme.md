This is the prototype for the "Polina-App" done by the 1# team of #Hack4Ukraine in Ulm.

To run you will need to do the following:

Install prerequisites:
- node
- docker
- pnpm
- an android/ios emulator or expo-app on your phone


Add an .env file in the backend folder with the following entries: \
`MONGODB_URI="mongodb://localhost:27017/polinadb"` \
`PORT=3001` \
`OPENAI_API_KEY={YOUR_API_KEY}` - If you have access to ChatGPT Api, only needed for translation

In the backend, run:
- pnpm i
- pnpm run mongo
- pnpm run seed (to get some content)
- pnpm run dev



\
This should be the last output then: 

`Server is running on port 3001 ` \
`Connected to MongoDB`

Add an .env file in the client folder with the following entries: \
`API_URL=http://localhost` \
`API_PORT=3001`

In the client, run:
- pnpm i
- pnpm start (and then press a for emulator or scan QR code with your expo-app)

