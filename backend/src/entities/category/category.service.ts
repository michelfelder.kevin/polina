import { Category, ITranslatedCategory } from "./category.model"

export class CategoryService {
	async getAllTranslatedCategories(
		language: string
	): Promise<ITranslatedCategory[]> {
		const categories = await Category.find()
		const translatedCategories: ITranslatedCategory[] = categories.map(
			(category) => {
				const { languageTexts } = category
				const translatedText =
					languageTexts.find(
						(languageText) => languageText.language === language
					)?.text || ""
				return {
					emoji: category.emoji,
					categoryId: category.categoryId,
					translatedText,
				}
			}
		)
		return translatedCategories
	}
}
