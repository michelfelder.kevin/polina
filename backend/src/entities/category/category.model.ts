import mongoose, { Document, Schema } from "mongoose"
import { ILanguageText } from "../article/article.model"
export interface INewCategory {
	emoji: string
	languageTexts: ILanguageText[]
	categoryId: string
}
export interface ITranslatedCategory
	extends Omit<INewCategory, "languageTexts"> {
	translatedText: string
}
export interface ICategory extends Document, INewCategory {}
const CategorySchema: Schema = new Schema({
	emoji: { type: String },
	languageTexts: [
		{
			language: { type: String },
			text: { type: String },
		},
	],
	categoryId: { type: String },
})
export const Category = mongoose.model<ICategory>("Category", CategorySchema)
