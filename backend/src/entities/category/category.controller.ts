import { Controller, Get, Path, Route } from "tsoa"
import { ITranslatedCategory } from "./category.model"
import { CategoryService } from "./category.service"

@Route("categories")
export class CategoryController extends Controller {
	private categoryService: CategoryService
	constructor() {
		super()
		this.categoryService = new CategoryService()
	}
	@Get("{language}")
	public async getAllTranslatedCategories(
		@Path() language: string
	): Promise<ITranslatedCategory[]> {
		return await this.categoryService.getAllTranslatedCategories(language)
	}
}
