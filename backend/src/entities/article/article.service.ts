import { randomUUID } from "crypto"
import { getLanguageTexts } from "../../helper"
import {
	Article,
	IArticle,
	IComment,
	ICreatedArticle,
	ICreatedLanguageArticle,
	ILanguageText,
	INewArticle,
	ITranslatedArticle,
} from "./article.model"
import { ApiError } from "../../helper/error"
const createArticle = async (articleData: ICreatedArticle) => {
	const createdArticle: ICreatedArticle = articleData
	const newArticle: INewArticle = {
		articleId: randomUUID(),
		categories: createdArticle.categories,
		comments: [],
		date: createdArticle.date,
		location: createdArticle.location,
		languageTexts: [
			{
				text: createdArticle.originalText,
				language: createdArticle.originalLanguage,
			},
		],
		languageTitle: [
			{
				text: createdArticle.originalTitle,
				language: createdArticle.originalLanguage,
			},
		],
		voters: {
			upvotedBy: [],
			downvotedBy: [],
		},
		address: createdArticle.address,
		openingHours: createdArticle.openingHours,
		author: createdArticle.author,
	}
	const article: IArticle = new Article(newArticle)
	try {
		const newArticle = await article.save()
		return newArticle.articleId
	} catch (error) {
		return null
	}
}
const createNewComment = async (
	articleId: string,
	comment: ILanguageText,
	username: string
) => {
	const article = await Article.findOne({ articleId })
	if (!article) {
		throw new ApiError(404, "Article not found")
	}
	const newComment: IComment = {
		date: new Date(),
		id: randomUUID(),
		languageTexts: [comment],
		voters: {
			upvotedBy: [],
			downvotedBy: [],
		},
		username,
	}
	article.comments.push(newComment)
	const savedArticle = await article.save()
	console.log(savedArticle.comments)
	return savedArticle.comments
}
const updateCommentWithTranslations = (
	articleId: string,
	originalLanguage: string,
	originalText: string,
	commentId: string
) => {
	try {
		getLanguageTexts(originalLanguage, originalText).then((languageText) =>
			Article.findOneAndUpdate(
				{ articleId, "comments.id": commentId },
				{
					$push: {
						"comments.$.languageTexts": {
							$each: languageText,
						},
					},
				}
			)
		)
	} catch (error) {
		console.log("Something went wrong translating the comments: ", error)
	}
}
const updateArticleWithTranslations = (
	articleId: string,
	originalLanguage: string,
	originalText: string,
	originalTitle: string
) => {
	try {
		getLanguageTexts(originalLanguage, originalText).then((languageTexts) =>
			Article.findOneAndUpdate(
				{ articleId },
				{
					$push: { languageTexts: { $each: languageTexts } },
				}
			)
		)
		getLanguageTexts(originalLanguage, originalTitle).then(
			(languageTitles) =>
				Article.findOneAndUpdate(
					{ articleId },
					{
						$push: { languageTitle: { $each: languageTitles } },
					}
				)
		)
	} catch (error) {
		console.log("Something went wrong translating the articles: ", error)
	}
}
export class ArticleService {
	async getAllTranslatedArticles(
		language: string
	): Promise<ITranslatedArticle[]> {
		const articles = await Article.find()
		const translatedArticles: ITranslatedArticle[] = articles.map(
			(article) => {
				const translatedText =
					article.languageTexts.find(
						(languageText) => languageText.language === language
					)?.text || ""
				const translatedTitle =
					article.languageTitle.find(
						(languageText) => languageText.language === language
					)?.text || ""
				return {
					articleId: article.articleId,
					categories: article.categories,
					comments: article.comments,
					date: article.date,
					location: article.location,
					translatedTitle,
					translatedText,
					voters: article.voters,
					address: article.address,
					openingHours: article.openingHours,
					author: article.author,
				}
			}
		)
		return translatedArticles
	}

	async getTranslatedArticleById(
		articleId: string,
		language: string
	): Promise<ITranslatedArticle | null> {
		const article = await Article.findOne({ articleId })
		if (!article) {
			return null
		}
		const { languageTexts, languageTitle } = article
		const translatedText =
			languageTexts.find(
				(languageText) => languageText.language === language
			)?.text ?? ""
		const translatedTitle =
			languageTitle.find(
				(languageText) => languageText.language === language
			)?.text ?? ""
		const translatedArticle: ITranslatedArticle = {
			articleId: article.articleId,
			categories: article.categories,
			comments: article.comments,
			date: article.date,
			location: article.location,
			translatedTitle,
			translatedText,
			voters: article.voters,
			address: article.address,
			openingHours: article.openingHours,
			author: article.author,
		}
		return translatedArticle
	}

	async createNewArticle(
		articleData: ICreatedLanguageArticle
	): Promise<string | null> {
		const articleId = await createArticle(articleData)
		if (articleId) {
			updateArticleWithTranslations(
				articleId,
				articleData.originalLanguage,
				articleData.originalText,
				articleData.originalTitle
			)
		}
		return articleId
	}

	async createArticleComment(
		articleId: string,
		comment: ILanguageText,
		username: string
	): Promise<IComment[]> {
		const originalLanguage = comment.language
		const originalText = comment.text
		const newComment = await createNewComment(articleId, comment, username)
		if (newComment) {
			updateCommentWithTranslations(
				articleId,
				originalLanguage,
				originalText,
				newComment[newComment.length - 1].id
			)
		}
		return newComment
	}

	async voteArticleById(
		articleId: string,
		isUpvote: boolean,
		userId: string
	): Promise<IArticle> {
		const article = await Article.findOne({ articleId })
		if (!article) {
			throw new ApiError(404, "Article not found")
		}
		if (isUpvote) {
			article.voters.upvotedBy.push(userId)
		} else {
			article.voters.downvotedBy.push(userId)
		}
		const updatedArticle = await article.save()
		return updatedArticle
	}

	async voteCommentById(
		commentId: string,
		isUpvote: boolean,
		userId: string
	): Promise<IArticle> {
		const article = await Article.findOne({ "comments.id": commentId })
		if (!article) {
			throw new ApiError(404, "Article not found")
		}
		const commentIndex = article.comments.findIndex(
			(comment) => comment.id === commentId
		)
		if (commentIndex < 0) {
			throw new ApiError(404, "Comment not found")
		}
		if (isUpvote) {
			article.comments[commentIndex].voters.upvotedBy.push(userId)
		} else {
			article.comments[commentIndex].voters.downvotedBy.push(userId)
		}
		const updatedArticle = await article.save()
		return updatedArticle
	}
}
