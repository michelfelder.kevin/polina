import mongoose, { Document, Schema } from "mongoose"
export interface INewArticle {
	articleId: string
	languageTitle: ILanguageText[]
	categories: string[]
	date: Date
	address?: string
	openingHours?: string
	location: string
	voters: {
		upvotedBy: string[]
		downvotedBy: string[]
	}
	comments: IComment[]
	languageTexts: ILanguageText[]
	author: string
}
export interface ITranslatedArticle
	extends Omit<INewArticle, "languageTexts" | "languageTitle"> {
	translatedTitle: string
	translatedText: string
}
export interface ICreatedArticle {
	categories: string[]
	address?: string
	openingHours?: string
	location: string
	date: Date
	originalTitle: string
	originalText: string
	author: string
	originalLanguage: string
}
export interface ICreatedLanguageArticle {
	categories: string[]
	address?: string
	openingHours?: string
	location: string
	date: Date
	originalTitle: string
	originalText: string
	author: string
	originalLanguage: string
}
export interface IComment {
	username: string
	languageTexts: ILanguageText[]
	voters: {
		upvotedBy: string[]
		downvotedBy: string[]
	}
	date: Date
	id: string
}
export interface ILanguageText {
	language: string
	text: string
}
export interface IArticle extends Document, INewArticle {}
const ArticleSchema: Schema = new Schema({
	articleId: { type: String },
	languageTitle: [
		{
			language: { type: String },
			text: { type: String },
		},
	],
	categories: { type: Array },
	date: { type: Date },
	address: { type: String },
	openingHours: { type: String },
	location: { type: String },
	voters: {
		upvotedBy: { type: Array },
		downvotedBy: { type: Array },
	},
	comments: [
		{
			username: { type: String },
			languageTexts: [
				{
					language: { type: String },
					text: { type: String },
				},
			],
			voters: {
				upvotedBy: { type: Array },
				downvotedBy: { type: Array },
			},
			date: { type: Date },
			id: { type: String },
		},
	],
	languageTexts: [
		{
			language: { type: String },
			text: { type: String },
		},
	],
	author: { type: String },
})
export const Article = mongoose.model<IArticle>("Article", ArticleSchema)
