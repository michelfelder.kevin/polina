import { Controller, Get, Path, Route, Post, Body, Put } from "tsoa"
import {
	IArticle,
	IComment,
	ICreatedLanguageArticle,
	ILanguageText,
	ITranslatedArticle,
} from "./article.model"
import { ArticleService } from "./article.service"
interface IVoteBody {
	isUpvote: boolean
	userId: string
}
interface ICommentBody {
	comment: ILanguageText
	username: string
}
@Route("articles")
export class ArticleController extends Controller {
	private articleService: ArticleService
	constructor() {
		super()
		this.articleService = new ArticleService()
	}
	@Get("{language}")
	public async getAllTranslatedArticles(
		@Path() language: string
	): Promise<ITranslatedArticle[]> {
		return await this.articleService.getAllTranslatedArticles(language)
	}
	@Get("{articleId}/{language}")
	public async getTranslatedArticle(
		@Path() articleId: string,
		@Path() language: string
	): Promise<ITranslatedArticle | null> {
		return await this.articleService.getTranslatedArticleById(
			articleId,
			language
		)
	}
	@Post("create")
	public async createNewArticle(
		@Body() articleData: ICreatedLanguageArticle
	): Promise<string | null> {
		return await this.articleService.createNewArticle(articleData)
	}
	@Put("comment/{articleId}")
	public async commentArticle(
		@Path() articleId: string,
		@Body() commentBody: ICommentBody
	): Promise<IComment[]> {
		console.log("here")
		return await this.articleService.createArticleComment(
			articleId,
			commentBody.comment,
			commentBody.username
		)
	}
	@Put("vote/{articleId}")
	public async voteArticle(
		@Path() articleId: string,
		@Body() vote: IVoteBody
	): Promise<IArticle> {
		return await this.articleService.voteArticleById(
			articleId,
			vote.isUpvote,
			vote.userId
		)
	}
	@Put("comments/vote/{commentId}")
	public async voteComment(
		@Path() commentId: string,
		@Body() vote: IVoteBody
	): Promise<IArticle> {
		return await this.articleService.voteCommentById(
			commentId,
			vote.isUpvote,
			vote.userId
		)
	}
}
