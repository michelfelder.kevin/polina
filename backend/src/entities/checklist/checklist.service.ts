import {
	Checklist,
	ITranslatedChecklist,
	ITranslatedChecklistStep,
} from "./checklist.model"
import { ApiError } from "../../helper/error"

export class ChecklistService {
	async getTranslatedChecklist(
		checklistId: string,
		language: string
	): Promise<ITranslatedChecklist> {
		const checklist = await Checklist.findOne({ checklistId })
		if (!checklist) {
			throw new ApiError(404, "Checklist not found")
		}
		const translatedSteps: ITranslatedChecklistStep[] = checklist.steps.map(
			(step) => {
				const translatedStep: ITranslatedChecklistStep = {
					translatedTitle:
						step.title.find(
							(languageText) => languageText.language === language
						)?.text || "",
					translatedSubTitle:
						step.subTitle.find(
							(languageText) => languageText.language === language
						)?.text || "",
					articleId: step.articleId,
					icon: step.icon,
					number: step.number,
				}
				return translatedStep
			}
		)
		const translatedChecklist: ITranslatedChecklist = {
			checklistId: checklist.checklistId,
			location: checklist.location,
			createdAt: checklist.createdAt,
			translatedSteps,
		}
		return translatedChecklist
	}
}
