import mongoose, { Document, Schema } from "mongoose"
import { ILanguageText } from "../article/article.model"
export interface INewChecklist {
	checklistId: string
	location: string
	createdAt: Date
	steps: IChecklistStep[]
}
export interface IChecklistStep {
	number: number
	articleId?: string
	title: ILanguageText[]
	subTitle: ILanguageText[]
	icon: string
}
export interface ITranslatedChecklist extends Omit<INewChecklist, "steps"> {
	translatedSteps: ITranslatedChecklistStep[]
}
export interface ITranslatedChecklistStep
	extends Omit<IChecklistStep, "title" | "subTitle"> {
	translatedTitle: string
	translatedSubTitle: string
}
export interface IChecklist extends Document, INewChecklist {}
const ChecklistSchema: Schema = new Schema({
	checklistId: { type: String },
	location: { type: String },
	createdAt: { type: Date },
	steps: [
		{
			number: { type: Number },
			articleId: { type: String },
			title: [
				{
					language: { type: String },
					text: { type: String },
				},
			],
			subTitle: [
				{
					language: { type: String },
					text: { type: String },
				},
			],
			icon: { type: String },
		},
	],
})
export const Checklist = mongoose.model<IChecklist>(
	"Checklist",
	ChecklistSchema
)
