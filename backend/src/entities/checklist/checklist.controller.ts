import { Route, Controller, Path, Get } from "tsoa"
import { ChecklistService } from "./checklist.service"
import { ITranslatedChecklist } from "./checklist.model"

@Route("checklists")
export class ChecklistController extends Controller {
	private checklistService: ChecklistService
	constructor() {
		super()
		this.checklistService = new ChecklistService()
	}
	@Get("{checklistId}/{language}")
	public async getTranslatedChecklist(
		@Path() checklistId: string,
		@Path() language: string
	): Promise<ITranslatedChecklist> {
		return await this.checklistService.getTranslatedChecklist(
			checklistId,
			language
		)
	}
}
