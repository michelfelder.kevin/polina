/* eslint-disable no-unused-vars */
import mongoose, { Document, Schema } from "mongoose"
import bcrypt from "bcrypt"
export interface IUserBody {
	name: string
	email?: string
	password: string
}
export interface INewUser {
	userId: string
	name: string
	password: string
	jwt: string
	role: EUserRole
	points: number
	email: string
	checklistAtStep: number
}
export enum EUserRole {
	user = "user",
	moderator = "moderator",
	admin = "administrator",
}
export interface IUser extends Document, INewUser {}
const UserSchema: Schema = new Schema({
	userId: { type: String },
	name: { type: String },
	password: { type: String, required: true },
	jwt: { type: String },
	role: { type: String },
	points: { type: Number },
	checklistAtStep: { type: Number },
})
UserSchema.pre<IUser>("save", function (next) {
	if (!this.isModified("password")) return next()
	bcrypt.hash(this.password, 10, (err, hash) => {
		if (err) return next(err)
		this.password = hash
		next()
	})
})
export const User = mongoose.model<IUser>("User", UserSchema)
