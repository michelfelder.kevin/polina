import { Body, Controller, Get, Path, Post, Put, Route } from "tsoa"
import { IUser, IUserBody } from "./user.model"
import { UserService } from "./user.service"

@Route("users")
export class UserController extends Controller {
	private userService: UserService
	constructor() {
		super()
		this.userService = new UserService()
	}
	@Get("{jwt}")
	public async getUserByJwt(@Path() jwt: string): Promise<IUser | null> {
		return await this.userService.getUserByJwt(jwt)
	}
	@Post("register")
	public async registerUser(@Body() userBody: IUserBody): Promise<IUser> {
		return await this.userService.registerUser(userBody)
	}
	@Post("login")
	public async loginUser(@Body() userBody: IUserBody): Promise<IUser> {
		return await this.userService.loginUser(userBody)
	}
	@Put("checklist/{userId}")
	public async setUserChecklistStep(
		@Body() atStep: number,
		@Path() userId: string
	): Promise<void> {
		return await this.userService.setUserChecklistStep(userId, atStep)
	}
}
