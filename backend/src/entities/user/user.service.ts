import { ApiError } from "../../helper/error"
import { EUserRole, IUser, IUserBody, User } from "./user.model"
import { randomUUID } from "crypto"
import jwt from "jsonwebtoken"
import bcrypt from "bcrypt"

export class UserService {
	async getUserByJwt(jwt: string): Promise<IUser | null> {
		const user = await User.findOne({ jwt })
		if (!user) {
			throw new ApiError(404, "User not found")
		}
		return user
	}
	async registerUser({ name, password, email }: IUserBody): Promise<IUser> {
		const foundUser = await User.findOne({ name })
		if (foundUser) {
			throw new ApiError(403, "Username already exists")
		}
		const userId = randomUUID()
		const token = jwt.sign({ userId: userId }, "supersecuresecret", {
			expiresIn: "100y",
		})
		const user = new User({
			name,
			password,
			userId: userId,
			jwt: token,
			role: EUserRole.user,
			points: 0,
			email,
			checklistAtStep: 1,
		})
		await user.save()
		console.log(user)
		return user
	}
	async loginUser({ name, password }: IUserBody): Promise<IUser> {
		const user = await User.findOne({ name })
		if (!user) {
			throw new ApiError(404, "User not found")
		}
		const isMatch = await bcrypt.compare(password, user.password)
		if (!isMatch) {
			throw new ApiError(400, "Credentials do not match")
		}
		return user
	}
	async setUserChecklistStep(userId: string, atStep: number): Promise<void> {
		const user = await User.findOne({ userId })
		if (!user) {
			throw new ApiError(404, "User not found")
		}
		user.checklistAtStep = atStep
		await user.save()
	}
}
