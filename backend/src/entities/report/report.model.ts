import mongoose, { Document, Schema } from "mongoose"
export interface INewReport {
	reportId: string
	reason: string
	type: string
	targetId: string
}

export interface IReport extends Document, INewReport {}
const ReportSchema: Schema = new Schema({
	reportId: { type: String },
	reason: { type: String },
	type: { type: String },
	targetId: { type: String },
})
export const UserReport = mongoose.model<IReport>("Report", ReportSchema)
