import { Route, Controller, Path, Post, Body } from "tsoa"
import { ReportService } from "./report.service"
interface IReportBody {
	reason: string
}
@Route("reports")
export class ReportController extends Controller {
	private reportService: ReportService
	constructor() {
		super()
		this.reportService = new ReportService()
	}
	@Post("comment/{commentId}")
	public async reportCommentById(
		@Path() commentId: string,
		@Body() reportBody: IReportBody
	): Promise<void> {
		return await this.reportService.reportCommentById(
			commentId,
			reportBody.reason
		)
	}
	@Post("article/{articleId}")
	public async reportArticleById(
		@Path() articleId: string,
		@Body() reportBody: IReportBody
	): Promise<void> {
		return await this.reportService.reportArticleById(
			articleId,
			reportBody.reason
		)
	}
}
