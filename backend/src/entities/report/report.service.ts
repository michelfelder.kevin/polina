import { randomUUID } from "crypto"
import { UserReport } from "./report.model"

export class ReportService {
	async reportCommentById(commentId: string, reason: string): Promise<void> {
		const newReport = new UserReport({
			reportId: randomUUID(),
			reason,
			targetId: commentId,
			type: "Comment",
		})
		await newReport.save()
	}
	async reportArticleById(articlId: string, reason: string): Promise<void> {
		const newReport = new UserReport({
			reportId: randomUUID(),
			reason,
			targetId: articlId,
			type: "Article",
		})
		await newReport.save()
	}
}
