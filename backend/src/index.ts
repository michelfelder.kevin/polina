import express, {
	urlencoded,
	Request as ExRequest,
	Response as ExResponse,
	NextFunction,
} from "express"
import dotenv from "dotenv"
import mongoose from "mongoose"
import { RegisterRoutes } from "./routes/routes"
import { ApiError } from "./helper/error"
import { ValidateError } from "tsoa"

dotenv.config()
const app = express()
const port = process.env.PORT || 3000

mongoose
	.connect(process.env.MONGODB_URI!)
	.then(() => console.log("Connected to MongoDB"))
	.catch((err: Error) => console.log(err))
// app.use(cors())
app.use(
	urlencoded({
		extended: true,
	})
)
app.use(express.json())
app.use((req, res, next) => {
	console.log(
		`${new Date().toISOString()} - ${req.method} request to ${req.url}`
	)
	next()
})
RegisterRoutes(app)

app.use(function (
	err: ValidateError | ApiError | Error,
	req: ExRequest,
	res: ExResponse,
	next: NextFunction
) {
	if (err.name === "ValidateError") {
		const valError = err as ValidateError
		console.warn(
			`Caught Validation Error for ${req.path}:`,
			valError.fields
		)
		return res.status(422).json({
			message: "Validation Failed",
			details: valError.fields,
		})
	} else if (err instanceof ApiError) {
		return res.status(err.statusCode).json({
			message: err.message,
		})
	} else if (err instanceof Error) {
		console.log(err)
		return res.status(500).json({
			message: "Internal server error",
		})
	}
	next()
})
app.listen(port, () => {
	console.log(`Server is running on port ${port}`)
})
