/* eslint-disable no-unused-vars */
import { INewCategory } from "../entities/category/category.model"

export enum ELanguages {
	german = "de",
	english = "en",
	ukrainian = "ua",
}
export const categories: INewCategory[] = [
	{
		emoji: "💊",
		categoryId: "1",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Medizin",
			},
			{
				language: ELanguages.ukrainian,
				text: "Медицина",
			},
			{
				language: ELanguages.english,
				text: "Medicine",
			},
		],
	},
	{
		emoji: "💸",
		categoryId: "2",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Kostenlos",
			},
			{
				language: ELanguages.ukrainian,
				text: "Безкоштовно",
			},
			{
				language: ELanguages.english,
				text: "Free",
			},
		],
	},
	{
		emoji: "🏠",
		categoryId: "3",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Wohnen",
			},
			{
				language: ELanguages.ukrainian,
				text: "Проживання",
			},
			{
				language: ELanguages.english,
				text: "Housing",
			},
		],
	},
	{
		emoji: "🚩",
		categoryId: "4",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Wichtig",
			},
			{
				language: ELanguages.ukrainian,
				text: "Важливо",
			},
			{
				language: ELanguages.english,
				text: "Important",
			},
		],
	},
	{
		emoji: "🇩🇪",
		categoryId: "5",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Sprachkurs",
			},
			{
				language: ELanguages.ukrainian,
				text: "Мовний курс",
			},
			{
				language: ELanguages.english,
				text: "Language Course",
			},
		],
	},
	{
		emoji: "👩‍💼",
		categoryId: "6",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Arbeit",
			},
			{
				language: ELanguages.ukrainian,
				text: "Робота",
			},
			{
				language: ELanguages.english,
				text: "Work",
			},
		],
	},
	{
		emoji: "🍕",
		categoryId: "7",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Lebensmittel",
			},
			{
				language: ELanguages.ukrainian,
				text: "Продукти харчування",
			},
			{
				language: ELanguages.english,
				text: "Food",
			},
		],
	},
	{
		emoji: "🤝",
		categoryId: "8",
		languageTexts: [
			{
				language: ELanguages.german,
				text: "Networking",
			},
			{
				language: ELanguages.ukrainian,
				text: "Мережеві зв'язки",
			},
			{
				language: ELanguages.english,
				text: "Networking",
			},
		],
	},
]
