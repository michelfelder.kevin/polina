import { EUserRole, INewUser } from "../entities/user/user.model"
export const users: INewUser[] = [
	{
		name: "Polina",
		email: "polina@polina.de",
		jwt: "abc",
		password: "polina",
		points: 1000,
		role: EUserRole.moderator,
		userId: "1",
		checklistAtStep: 1,
	},
]
