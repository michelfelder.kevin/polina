import { INewArticle } from "../entities/article/article.model"
import { faker } from "@faker-js/faker"
import { ELanguages } from "./categories"
export const articlesDe: INewArticle[] = [
    {
        "articleId": "1",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Infos zur medizinischen Versorgung vom Bundesgesundheitsministerium"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Інформація про медичне обслуговування від Федерального міністерства охорони здоров'я"
            },
            {
                "language": ELanguages.english,
                "text": "Information on Medical Care from the Federal Ministry of Health"
            }
        ],
        "categories": [
            "1",
            "2"
        ],
        "date": faker.date.recent(),
        "address": "Münsterplatz 123",
        "openingHours": "8:00 - 12:00 Uhr",
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 21 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 2 })
        },
        "comments": [
            {
                "username": "superuser1",
                "languageTexts": [
                    {
                        "language": ELanguages.german,
                        "text": "Sehr hilfreich, danke"
                    },
                    {
                        "language": ELanguages.ukrainian,
                        "text": "Дуже корисно, дякую"
                    },
                    {
                        "language": ELanguages.english,
                        "text": "Very helpful, thank you"
                    }
                ],
                "voters": {
                    upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 12 }),
                    downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
                },
                "date": faker.date.recent(),
                "id": "1"
            }
        ],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"
    },
    {
        "articleId": "2",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Aktuell werden keine weiteren Flüchtlinge aufgenommen"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Наразі більше біженців не приймаються"
            },
            {
                "language": ELanguages.english,
                "text": "No Further Refugees Currently Being Accepted"
            }
        ],
        "categories": [
"3",
"4"
],
        "date": faker.date.recent(),
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 12 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
        },
        "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"

    },
    {
        "articleId": "3",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Sprachkurse vor Ort"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Мовні курси на місці"
            },
            {
                "language": ELanguages.english,
                "text": "Local Language Courses"
            }
        ],
        "categories": [
"5",
"2"
],
        "date": faker.date.recent(),
        "address": "Augsburger Str. 12",
        "openingHours": "15:00 - 21:00 Uhr",
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 14 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
        },
        "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"

    },
    {
        "articleId": "4",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Jobs für ukrainische Geflüchtete"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Робота для українських біженців"
            },
            {
                "language": ELanguages.english,
                "text": "Jobs for Ukrainian Refugees"
            }
        ],
        "categories": ["6"],
        "date": faker.date.recent(),
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 10 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
        },
        "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"

    },
    {
        "articleId": "5",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Kostenlose Essensausgabe"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Безкоштовна видача їжі"
            },
            {
                "language": ELanguages.english,
                "text": "Free Food Distribution"
            }
        ],
        "categories": [
"7",
"2"
],
        "date": faker.date.recent(),
        "address": "Münsterplatz 123",
        "openingHours": "8:00 - 12:00 Uhr",
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 5 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
        }, "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"

    },
    {
        "articleId": "6",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Zentrales Ankunftszentrum in der Messehalle 6"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Центральний приймальний центр у виставковій залі 6"
            },
            {
                "language": ELanguages.english,
                "text": "Central Arrival Center in Exhibition Hall 6"
            }
        ],
        "categories": ["3"],
        "date": faker.date.recent(),
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 6 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
        }, "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"

    },
    {
        "articleId": "7",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Flohmarkt in Stuttgart"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Блошиний ринок у Штутгарті"
            },
            {
                "language": ELanguages.english,
                "text": "Flea Market in Stuttgart"
            }
        ],
        "categories": ["3"],
        "date": faker.date.recent(),
        "location": "Stuttgart",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 7 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
        }, "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"

    },
    {
        "articleId": "8",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Kostenlose SIM-Karten der Telekom und Vodafone"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Безкоштовні SIM-карти від Telekom та Vodafone"
            },
            {
                "language": ELanguages.english,
                "text": "Free SIM Cards from Telekom and Vodafone"
            }
        ],
        "categories": [
"8",
"2"
],
        "date": faker.date.recent(),
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 10 })
        }, "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"

    },
    {
        "articleId": "9",
        "languageTitle": [
            {
                "language": ELanguages.german,
                "text": "Das Ankunftszentrum in Ulm"
            },
            {
                "language": ELanguages.ukrainian,
                "text": "Центр прибуття в Ульмі"
            },
            {
                "language": ELanguages.english,
                "text": "The Arrival Center in Ulm"
            }
        ],
        "categories": ["3"],
        "location": "Ulm/Neu-Ulm",
        "voters": {
            upvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 107 }),
            downvotedBy: faker.helpers.multiple(faker.person.firstName, { count: 1 })
        },
        "date": faker.date.recent(),
        "comments": [],
        "languageTexts": [
            {
                "language": ELanguages.german,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.ukrainian,
                "text": faker.lorem.lines()
            },
            {
                "language": ELanguages.english,
                "text": faker.lorem.lines()
            }
        ],
        author: "Polina"
    }
]