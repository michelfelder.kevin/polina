import { INewChecklist } from "../entities/checklist/checklist.model"

export const checklistUlmDe: INewChecklist = {
	checklistId: "1",
	createdAt: new Date(),
	location: "Ulm/Neu-Ulm",
	steps: [
		{
			title: [
				{ language: "de", text: "Bleibe finden" },
				{ language: "en", text: "Find Accommodation" },
				{ language: "ua", text: "Знайти житло" },
			],
			subTitle: [
				{ language: "de", text: "das Ankunftszentrum" },
				{ language: "en", text: "the Arrival Center" },
				{ language: "ua", text: "Центр прибуття" },
			],
			icon: "🏠",
			number: 1,
			articleId: "9",
		},
		{
			title: [
				{ language: "de", text: "Anmeldung" },
				{ language: "en", text: "Registration" },
				{ language: "ua", text: "Реєстрація" },
			],
			subTitle: [
				{ language: "de", text: "die Meldebestätigung" },
				{ language: "en", text: "the Registration Confirmation" },
				{ language: "ua", text: "Підтвердження реєстрації" },
			],
			icon: "🪪",
			number: 2,
		},
		{
			title: [
				{ language: "de", text: "Amt" },
				{ language: "en", text: "Office" },
				{ language: "ua", text: "Офіс" },
			],
			subTitle: [
				{ language: "de", text: "das Sozialamt, die Ausländerbehörde" },
				{
					language: "en",
					text: "the Social Welfare Office, the Foreigners' Authority",
				},
				{
					language: "ua",
					text: "Соціальне бюро, орган іноземних справ",
				},
			],
			icon: "🏛️",
			number: 3,
		},
		{
			title: [
				{ language: "de", text: "Jobcenter" },
				{ language: "en", text: "Job Center" },
				{ language: "ua", text: "Центр зайнятості" },
			],
			subTitle: [
				{ language: "de", text: "Onlineanmeldung" },
				{ language: "en", text: "Online Registration" },
				{ language: "ua", text: "Онлайн реєстрація" },
			],
			icon: "💼",
			articleId: "9",
			number: 4,
		},
		{
			title: [
				{ language: "de", text: "Sozial" },
				{ language: "en", text: "Social" },
				{ language: "ua", text: "Соціальний" },
			],
			subTitle: [
				{ language: "de", text: "für Menschen über 67 Jahre" },
				{ language: "en", text: "for People over 67 Years" },
				{ language: "ua", text: "для людей старше 67 років" },
			],
			icon: "📝",
			number: 5,
		},
		{
			title: [
				{ language: "de", text: "Versicherungen" },
				{ language: "en", text: "Insurances" },
				{ language: "ua", text: "Страхування" },
			],
			subTitle: [
				{ language: "de", text: "AOK, BBK und weitere" },
				{ language: "en", text: "AOK, BBK and others" },
				{ language: "ua", text: "AOK, BBK та інші" },
			],
			icon: "🏥",
			number: 6,
		},
		{
			title: [
				{ language: "de", text: "Familienkasse" },
				{ language: "en", text: "Family Benefits Office" },
				{ language: "ua", text: "Офіс сімейних виплат" },
			],
			subTitle: [
				{ language: "de", text: "Babys und Kinder bis 18 Jahre" },
				{ language: "en", text: "Babies and Children up to 18 Years" },
				{ language: "ua", text: "Немовлята та діти до 18 років" },
			],
			icon: "👨‍👩‍👧‍👦",
			number: 7,
		},
		{
			title: [
				{ language: "de", text: "Sprachkurs" },
				{ language: "en", text: "Language Course" },
				{ language: "ua", text: "Мовний курс" },
			],
			subTitle: [
				{ language: "de", text: "Sprachschulen, BAMF" },
				{ language: "en", text: "Language Schools, BAMF" },
				{ language: "ua", text: "Мовні школи, BAMF" },
			],
			icon: "🗣️",
			number: 8,
		},
		{
			title: [
				{ language: "de", text: "Tiere" },
				{ language: "en", text: "Animals" },
				{ language: "ua", text: "Тварини" },
			],
			subTitle: [
				{ language: "de", text: "Haustiere und Tierärzte" },
				{ language: "en", text: "Pets and Veterinarians" },
				{ language: "ua", text: "Домашні тварини та ветеринари" },
			],
			icon: "🐾",
			number: 9,
		},
	],
}
