import { getTranslation } from "../autoTranslation"
import { ELanguages } from "../data/categories"
import { ILanguageText } from "../entities/article/article.model"

export const getLanguageKeyByValue = (value: string): string | undefined => {
	console.log(value)
	for (const language in ELanguages) {
		if (ELanguages[language as keyof typeof ELanguages] === value) {
			return language
		}
	}
	return undefined // Add proper error handling
}
export const getLanguageTexts = async (
	originalLanguage: string,
	originalText: string
) => {
	const languageTexts: ILanguageText[] = []
	for (const language in ELanguages) {
		if (
			ELanguages[language as keyof typeof ELanguages] !== originalLanguage
		) {
			const text = (await getTranslation(language, originalText)) ?? "" // Add proper error handling
			languageTexts.push({
				language: ELanguages[language as keyof typeof ELanguages],
				text,
			})
		}
	}
	return languageTexts
}
