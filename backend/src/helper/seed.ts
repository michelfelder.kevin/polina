import mongoose from "mongoose"
import dotenv from "dotenv"
import { articlesDe } from "../data/articles"
import { Article } from "../entities/article/article.model"
import { checklistUlmDe } from "../data/checklists"
import { Checklist } from "../entities/checklist/checklist.model"
import { categories } from "../data/categories"
import { User } from "../entities/user/user.model"
import { users } from "../data/users"
import { Category } from "../entities/category/category.model"
dotenv.config()
mongoose
	.connect(process.env.MONGODB_URI!)
	.then(() => console.log("Connected to MongoDB"))
	.catch((err: Error) => console.log(err))
const getUserSeeding = async () => {
	const userPromises = users.map((user) => {
		const newUser = new User(user)
		return newUser.save()
	})
	await Promise.all(userPromises)
}
const getChecklistSeeding = async () => {
	const newChecklist = new Checklist(checklistUlmDe)
	return await newChecklist.save()
}
const getCategorySeeding = async () => {
	const categoryPromises = categories.map((category) => {
		const newCategory = new Category(category)
		return newCategory.save()
	})
	await Promise.all(categoryPromises)
}
const getArticleSeeding = async () => {
	const articlePromises = articlesDe.map((article) => {
		const newArticle = new Article(article)
		return newArticle.save()
	})
	await Promise.all(articlePromises)
}
getUserSeeding()
getChecklistSeeding()
getCategorySeeding()
getArticleSeeding().then(() => {
	mongoose.connection.close()
})
console.log("Database seeded!")
