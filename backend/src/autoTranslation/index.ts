import OpenAI from "openai";
import dotenv from 'dotenv';
dotenv.config()
const openai = new OpenAI({
    apiKey: process.env.OPENAI_API_KEY || "",
})
export const getTranslation = async (language: string, text: string) => {
    if (openai.apiKey === "") {
        return null
    }
    console.log("ChatGPT is translating into ", language)
    const translation: OpenAI.ChatCompletion = await openai.chat.completions.create({
        messages: [
            { role: "system", content: "You are a helpful assistant designed to translate messages. Always use the same punctuation as the original given input. Only answer with the translation, don't add anything else to it." },
            { role: "system", content:  `Translate the next message you will receive into ${language}`},
            { role: "user", content: `${text}` }
        ],
        model: "gpt-3.5-turbo-1106",
    });
    const translationContent = translation['choices'][0]['message']['content']
    return translationContent
}