import axios from "axios"
import { IArticle, IComment, ICreatedLanguageArticle, ILanguageTexts, ITranslatedArticle } from "../types/article"
import { IChecklist, ITranslatedChecklist } from "../types/checklist"
import { ICategory, ITranslatedCategory } from "../types/category"
import { API_PORT, API_URL } from "@env"


export const apiGetAllArticles = async () => {
    const allArticles: IArticle[] = await axios.get(`${API_URL}:${API_PORT}/articles`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return allArticles
}
export const apiGetAllTranslatedArticles = async (language: string) => {
    const allTranslatedArticles: ITranslatedArticle[] = await axios.get(`${API_URL}:${API_PORT}/articles/${language}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return allTranslatedArticles
}
export const apiGetAllCategories = async () => {
    const allCategories: ICategory[] = await axios.get(`${API_URL}:${API_PORT}/categories`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return allCategories
}
export const apiGetAllTranslatedCategories = async (language: string) => {
    const allTranslatedCategories: ITranslatedCategory[] = await axios.get(`${API_URL}:${API_PORT}/categories/${language}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return allTranslatedCategories
}

export const apiGetUserByJWT = async (jwt: string) => {
    const user = await axios.get(`${API_URL}:${API_PORT}/users/${jwt}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return user
}
export const apiRegisterUser = async (name: string, password: string, email: string) => {
    const user: IUser = await axios.post(`${API_URL}:${API_PORT}/users/register`, { name, password, email })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return user
}
export const apiLoginUser = async (name: string, password: string) => {
    const user: IUser = await axios.post(`${API_URL}:${API_PORT}/users/login`, { name, password })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return user
}
export const apiGetArticle = async (articleId: string) => {
    const article: IArticle = await axios.get(`${API_URL}:${API_PORT}/articles/${articleId}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return article
}
export const apiGetTranslatedArticle = async (articleId: string, language: string) => {
    const article: ITranslatedArticle = await axios.get(`${API_URL}:${API_PORT}/articles/${articleId}/${language}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return article
}
export const apiCreateArticle = async (articleData: ICreatedLanguageArticle) => {
    const newArticleId: string = await axios.post(`${API_URL}:${API_PORT}/articles/create`, articleData )
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return newArticleId
}
export const apiSetUserAtStep = async (userId: string, atStep: number) => {
    await axios.put(`${API_URL}:${API_PORT}/user/${userId}`, { atStep })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
}
export const apiGetTranslatedChecklist = async (checklistId: string, language: string) => {
    const checklist: ITranslatedChecklist = await axios.get(`${API_URL}:${API_PORT}/checklists/${checklistId}/${language}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return checklist
}
export const apiAddCommentToArticle = async (articleId: string, comment: ILanguageTexts, username: string) => {
    const updatedArticleComments: IComment[] = await axios.put(`${API_URL}:${API_PORT}/articles/comment/${articleId}`, { comment, username })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return updatedArticleComments
}
export const apiVoteArticle = async (articleId: string, isUpvote: boolean, userId: string) => {
    const updatedArticle: IArticle = await axios.put(`${API_URL}:${API_PORT}/articles/vote/${articleId}`, { isUpvote, userId })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return updatedArticle
}
export const apiVoteComment = async (commentId: string, isUpvote: boolean, userId: string) => {
    const updatedArticle: IArticle = await axios.put(`${API_URL}:${API_PORT}/comments/vote/${commentId}`, { isUpvote, userId })
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
    return updatedArticle
}
export const apiReportComment = async (commentId: string, reason: string) => {
    await axios.post(`${API_URL}:${API_PORT}/reports/comment/${commentId}`, reason )
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
}
export const apiReportArticle = async (articleId: string, reason: string) => {
    await axios.post(`${API_URL}:${API_PORT}/reports/article/${articleId}`, {reason} )
        .then(res => {
            return res.data
        })
        .catch(err => {
            console.log(err)
        })
}