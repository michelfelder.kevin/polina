import { ILanguageTexts } from "./article"

export interface IChecklist {
    checklistId: string
    location: string
    createdAt: Date
    steps: IChecklistStep[]
}
export interface ITranslatedChecklist extends Omit<IChecklist, "steps"> {
    translatedSteps: ITranslatedChecklistStep[]
}
export interface ITranslatedChecklistStep extends Omit<IChecklistStep, "title" | "subTitle"> {
    translatedTitle: string
    translatedSubTitle: string
}
export interface IChecklistStep {
    number: number
    articleId?: string
    title: ILanguageTexts[]
    subTitle: ILanguageTexts[]
    icon: string
}