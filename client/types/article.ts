import { ELanguages } from "../localization/languages"

export interface IArticle {
    articleId: string
    languageTitle: ILanguageTexts[]
    categories: string[]
    date: Date
    address?: string
    openingHours?: string
    location: string
    voters: {
        upvotedBy: string[]
        downvotedBy: string[]
    }
    comments: IComment[]
    languageTexts: ILanguageTexts[]
    author: string
}
export interface ICreatedLanguageArticle {
    categories: string[]
    address?: string
    openingHours?: string
    location: string
    date: Date
    originalTitle: string
    originalText: string
    author: string
    originalLanguage: string
}
export interface ITranslatedArticle extends Omit<IArticle, "languageTexts" | "languageTitle"> {
    translatedTitle: string
    translatedText: string
}
export interface ILanguageTexts {
    language: string
    text: string
}

export interface IComment {
    username: string,
    languageTexts: ILanguageTexts[],
    voters: {
        upvotedBy: string[]
        downvotedBy: string[]
    }
    date: Date,
    id: string
}

