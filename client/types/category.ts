import { ILanguageTexts } from "./article";

export interface ICategory {
    emoji: string,
    languageTexts: ILanguageTexts[],
    categoryId: string
}
export interface ITranslatedCategory extends Omit<ICategory, 'languageTexts'> {
    translatedText: string
}