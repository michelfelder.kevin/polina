interface IUser {
    userId: string,
    name: string,
    password: string,
    jwt: string,
    role: EUserRole,
    points: number,
    checklistAtStep: number
}
enum EUserRole {
    user = "user",
    moderator = "moderator",
    admin = "administrator"
}