export type RootStackParamList = {
    Home: undefined,
    Checklist: undefined,
    Article: { articleId: number },
    CreateArticle: undefined,
    Profile: undefined,
    Register: undefined,
    Login: undefined
};