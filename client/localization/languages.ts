export enum ELanguages {
    "🇩🇪" = "de",
    "🇺🇸" = "en",
    "🇺🇦" = "ua"
}