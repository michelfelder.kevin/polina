import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './en/common.json';
import de from './de/common.json';
import ua from './ua/common.json';
const resources = {
    en: { translation: en },
    de: { translation: de },
    ua: { translation: ua },
};
i18n.use(initReactI18next)
    .init({
        compatibilityJSON: 'v3', //To make it work for Android devices, add this line.
        resources,
        lng: 'en', // default language to use.
        // if you're using a language detector, do not define the lng option
        interpolation: {
            escapeValue: false,
        },
    });
export default i18n;