import { SafeAreaView } from "react-native-safe-area-context"
import { s } from "./styles"
import { useUser } from "../../provider/user"
import { TextInput, View, Text, TouchableOpacity } from "react-native"
import { useEffect, useState } from "react"
import { ParamListBase, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"
import { apiRegisterUser } from "../../api"
import * as SecureStore from 'expo-secure-store'
import { useTranslation } from "react-i18next"
export const RegisterScreen = () => {
    const { user, setUser } = useUser()
    const [name, setName] = useState("")
    const [password, setPassword] = useState("")
    const [passwordVerify, setPasswordVerify] = useState("")
    const [email, setEmail] = useState("")
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const { t } = useTranslation()
    const isValidEmail = (email: string) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
        return emailRegex.test(email)
    }
    const handleSignUpButton = async () => {
        if (name === "" || password === "" || email === "") {
            alert(t("alert.forms"))
            return
        } else if (password !== passwordVerify) {
            alert(t("alert.passwords"))
            return
        } else if (!isValidEmail(email)) {
            alert(t("alert.email"))
            return
        }
        const newUser = await apiRegisterUser(name, password, email)
        await SecureStore.setItemAsync("JWT", newUser.jwt)
        setUser(newUser)
    }
    useEffect(() => {
        if (user) {
            navigation.navigate("Home")
        }
    }, [user])
    return (
        <SafeAreaView style={s.container}>
            <Text style={s.title}>{t("register.registerNow")}</Text>
            <View>
                <TextInput style={s.input} placeholder={t("register.username")} onChangeText={(text) => setName(text.trim())} value={name} />
                <TextInput style={s.input} placeholder={t("register.password")} onChangeText={(text) => setPassword(text.trim())} value={password} secureTextEntry />
                <TextInput style={s.input} placeholder={t("register.verify")} onChangeText={(text) => setPasswordVerify(text.trim())} value={passwordVerify} secureTextEntry />
                <TextInput style={s.input} placeholder={t("register.email")} onChangeText={(text) => setEmail(text.trim())} value={email} />
            </View>
            <View style={s.buttons}>
                <TouchableOpacity style={s.loginButton} onPress={() => handleSignUpButton()}>
                    <Text style={s.buttonText}>{t("register.signUp")}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={s.guestButton} onPress={() => navigation.navigate("Home")}>
                    <Text style={s.buttonText}>{t("register.asGuest")}</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity style={s.noAccount} onPress={() => navigation.navigate("Login")}>
                <Text>{t("register.hasAccount")}</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

