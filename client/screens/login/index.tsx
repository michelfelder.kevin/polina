import { SafeAreaView } from "react-native-safe-area-context"
import { s } from "./styles"
import { useUser } from "../../provider/user"
import { TextInput, View, Text, TouchableOpacity } from "react-native"
import { useEffect, useState } from "react"
import { ParamListBase, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"
import { apiLoginUser, apiRegisterUser } from "../../api"
import * as SecureStore from 'expo-secure-store'
import { useTranslation } from "react-i18next"
export const LoginScreen = () => {
    const { user, setUser } = useUser()
    const [name, setName] = useState("")
    const [password, setPassword] = useState("")
    const { t } = useTranslation()
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const handleSignInButton = async () => {
        if (name === "" || password === "") {
            alert(t("alert.forms"))
            return
        } else {
            const loginUser = await apiLoginUser(name, password)
            if (!loginUser) {
                alert(t("alert.failedLogin"))
                return
            }
            await SecureStore.setItemAsync("JWT", loginUser.jwt)
            setUser(loginUser)
        }
    }
    useEffect(() => {
        if (user) {
            navigation.navigate("Home")
        }
    }, [user])
    return (
        <SafeAreaView style={s.container}>
            <Text style={s.title}>{t("login.loginNow")}</Text>
            <View>
                <TextInput style={s.input} placeholder={t("login.username")} onChangeText={(text) => setName(text.trim())} value={name} />
                <TextInput style={s.input} placeholder={t("login.password")} onChangeText={(text) => setPassword(text.trim())} value={password} secureTextEntry />
            </View>
            <View style={s.buttons}>
                <TouchableOpacity style={s.loginButton} onPress={handleSignInButton}>
                    <Text style={s.buttonText}>{t("login.login")}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={s.guestButton} onPress={() => navigation.navigate("Home")}>
                    <Text style={s.buttonText}>{t("login.asGuest")}</Text>
                </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate("Register")} style={s.noAccount}>
                <Text>{t("login.noAccount")}</Text>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

