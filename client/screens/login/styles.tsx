import { StyleSheet } from "react-native"
const backgroundColor = "#FBFBFB"
export const s = StyleSheet.create({
    container: {
        height: "100%",
        justifyContent: "center"
    },
    title: {
        textAlign: "center",
        marginTop: -100,
        fontSize: 20
    },
    input: {
        justifyContent: "center",
        marginLeft: 20,
        borderWidth: 1,
        borderColor: "lightgray",
        borderRadius: 20,
        padding: 5,
        marginRight: 20,
        marginTop: 30
    },
    buttons: {
        flexDirection: "row",
        justifyContent: "center",
        marginLeft: 20,
        marginRight: 20,
        marginTop: 50
    },
    buttonText: {
        color: "white"
    },
    loginButton: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#0056B9",
        marginRight: 10,
        padding: 5,
        borderRadius: 20
    },
    guestButton: {
        flex: 2,
        alignItems: "center",
        backgroundColor: "#0056B9",
        marginLeft: 10,
        padding: 5,
        borderRadius: 20
    },
    noAccount: {
        marginTop: 10,
        marginLeft: 20
    }
})