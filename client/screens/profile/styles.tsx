import { StyleSheet } from "react-native"
export const s = StyleSheet.create({
    profileContainer: {
        backgroundColor: "white",
        height: "100%"
    },
    profileImageBackground: {
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#0056B9",
        borderBottomWidth: 10,
        borderTopWidth: 5,
        borderColor: "lightgray"
    },
    profileHeader: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 10
    },
    profileImage: {
        height: 100,
        width: 100,
        borderRadius: 100,
        borderWidth: 2,
        borderColor: "violet",
        marginBottom: 10,
        marginTop: 10
    },
    profileUserStats: {
        flexDirection: "row",
        marginTop: 50,
        marginBottom: 10
    },
    empty: {
        flex: 1,
        width: 5,
    },
    settingsWrapper: {
        flex: 1,
        alignItems: "flex-end"
    },
    settingsImage: {
        width: 30,
        height: 30,
        marginRight: 10
    },
    name: {
        flex: 1,
        color: "white",
        fontSize: 20,
        textAlign: "center"
    },
    edit: {
        color: "white"
    },
    userStat: {
        flex: 1,
        textAlign: "center",
        color: "white"
    },
    userStatLeft: {
        flex: 1,
        textAlign: "left",
        marginLeft: 10,
        color: "white"
    },
    userStatRight: {
        flex: 1,
        textAlign: "right",
        marginRight: 10,
        color: "violet",
        fontStyle: "italic",
        fontWeight: "500"
    },
    userStatRightNormal: {
        flex: 1,
        textAlign: "right",
        marginRight: 10,
        color: "white",
    },
    recentActivityContainer: {
        backgroundColor: "#FBFBFB",
        height: "100%",
        width: "100%",
    },
    activityTitle: {
        fontSize: 20,
        marginTop: 10,
        paddingBottom: 10,
        paddingLeft: 20,
        borderBottomWidth: 1,
        borderColor: "lightgray",
    },
    activity: {
        flexDirection: "row",
        paddingTop: 5,
        paddingLeft: 20,
        width: "100%",
    },
    activityType: {
        fontStyle: "italic"
    },
    articleTitle: {
        fontWeight: "700"
    },
    date: {
        color: "gray"
    },
    logoutButton: {
        flex: 1,
        justifyContent: "center"
    },
    logout: {
        color: "white",
        marginLeft: 10,
        marginBottom: 4
    }
})