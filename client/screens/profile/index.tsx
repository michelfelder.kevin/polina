import { View, Text, Image, TouchableOpacity } from "react-native"
import { s } from "./styles"
import { useEffect } from "react"
import { useUser } from "../../provider/user"
import * as SecureStore from 'expo-secure-store';
import { ParamListBase, useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { useTranslation } from "react-i18next";
export const ProfileScreen = () => {
    const { user, setUser } = useUser()
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const { t } = useTranslation()
    const handlePressSettingsutton = async () => {
        await SecureStore.deleteItemAsync("JWT")
        setUser(undefined)
        navigation.navigate("Login")
    }
    return (
        <View style={s.profileContainer}>
            <View style={s.profileImageBackground}>
                <View style={s.profileHeader}>
                    <TouchableOpacity style={s.logoutButton} onPress={handlePressSettingsutton}>
                        <Text style={s.logout}>{user ? t("profile.logout"): t("profile.login")}</Text>
                    </TouchableOpacity>
                    <Text style={s.name}>{user ? user.name : t("profile.guest")}</Text>
                    <TouchableOpacity style={s.settingsWrapper} onPress={handlePressSettingsutton}>
                        <Image style={s.settingsImage} source={require("../../assets/settings.png")}></Image>
                    </TouchableOpacity>
                </View>
                <Image source={require("../../assets/polina.jpeg")} style={s.profileImage} />
                <Text style={s.edit}>{t("profile.edit")}</Text>
                <View style={s.profileUserStats}>
                    <Text style={s.userStatLeft}>0 {t("profile.friends")}</Text>
                    {/* <Text style={s.userStat}>{user?.points}</Text> */}
                    <Text style={user?.role === "moderator" || user?.role === "administrator" ? s.userStatRight : s.userStatRightNormal}>{user ? user.role.charAt(0).toUpperCase() + user.role.slice(1) : t("profile.guest")}</Text>
                </View>
            </View>
            <View style={s.recentActivityContainer}>
                <Text style={s.activityTitle}>{t("profile.recentActivities")}</Text>
                {/* <TouchableOpacity style={s.activity}>
                    <Text style={s.activityType}>{t("profile.comingSoon")}</Text>
                </TouchableOpacity> */}
                <TouchableOpacity style={s.activity}>
                    <Text style={s.activityType}>Created Article </Text>
                    <Text style={s.articleTitle}>The Arrival Center in Ulm</Text>
                    <Text style={s.date}> - 22.11.2023</Text>
                </TouchableOpacity>
                <TouchableOpacity style={s.activity}>
                    <Text style={s.activityType}>Commented on </Text>
                    <Text style={s.articleTitle}>Local Language Courses</Text>
                    <Text style={s.date}> - 21.11.2023</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}