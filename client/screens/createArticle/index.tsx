import { useState } from "react"
import { View, Text, TextInput, TouchableOpacity, Touchable, TouchableWithoutFeedback } from "react-native"
import { s } from "./styles"
import { ICategory, ITranslatedCategory } from "../../types/category"
import { useCategories } from "../../provider/category"
import { SelectableCategory } from "../../components/selectableCategory"
import { ICreatedLanguageArticle } from "../../types/article"
import { useTranslation } from "react-i18next"
import { ParamListBase, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"
import { apiCreateArticle } from "../../api"
import { useUser } from "../../provider/user"
export const CreateArticle = () => {
    const [title, setTitle] = useState("")
    const [text, setText] = useState("")
    const [address, setAddress] = useState("")
    const [openingHours, setOpeningHours] = useState("")
    const [selectedCategories, setSelectedCategories] = useState<ITranslatedCategory[]>([])
    const [isCategoryModalOpen, setIsCategoryModalOpen] = useState(false)
    const { categories } = useCategories()
    const { t, i18n } = useTranslation()
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const { user } = useUser()
    const handleSelectCategoryButton = (selectedCategory: ITranslatedCategory) => {
        setSelectedCategories(prevSelected => {
            if (prevSelected.includes(selectedCategory)) {
                return prevSelected.filter(category => category !== selectedCategory);
            } else {
                return [...prevSelected, selectedCategory];
            }
        })
    }
    const handleSaveButton = async () => {
        if (!user) return
        const newArticle: ICreatedLanguageArticle = {
            categories: selectedCategories.map((category) => category.categoryId),
            date: new Date,
            location: "Ulm/Neu-Ulm",
            address,
            openingHours,
            originalText: text,
            originalTitle: title,
            originalLanguage: i18n.language,
            author: user.name
        }
        const newArticleId = await apiCreateArticle(newArticle)
        navigation.navigate("Article", { articleId: newArticleId })
    }
    return (
        <View style={s.createArticleWrapper}>
            <TextInput style={s.title} onChangeText={(text) => setTitle(text)} value={title} placeholder={t("article.createTitle")}></TextInput>
            <TextInput style={s.adress} onChangeText={(text) => setAddress(text)} value={address} placeholder={t("article.createStreet")}></TextInput>
            <TextInput style={s.openingHours} onChangeText={(text) => setOpeningHours(text)} value={openingHours} placeholder="8:00 - 17:00"></TextInput>
            <View style={s.articleTextContainer} >
                <TextInput multiline style={s.articleText} onChangeText={(text) => setText(text)} value={text} placeholder={t("article.createText")}></TextInput>
            </View>
            <View>
                <TouchableOpacity onPress={() => setIsCategoryModalOpen(!isCategoryModalOpen)}>
                    <Text style={isCategoryModalOpen ? s.openedSelectedCategoriesText : s.selectCategoriesText}>
                        {t("article.categories")}
                    </Text>
                </TouchableOpacity>
                {isCategoryModalOpen &&
                    <View style={s.categories}>
                        {categories.map((category, index) => {
                            return (
                                <SelectableCategory category={category} key={index} onToggle={handleSelectCategoryButton} />
                            )
                        })}
                    </View>}
            </View>
            <View style={s.buttonsWrapper}>
                <TouchableOpacity style={s.button} onPress={() => navigation.navigate("Home")}>
                    <Text style={s.buttonText}>
                        {t("article.cancel")}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={s.button} disabled={title === "" || text === ""} onPress={() => handleSaveButton()}>
                    <Text style={s.buttonText}>
                        {t("article.save")}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}