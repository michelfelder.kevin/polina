import { StyleSheet } from "react-native"
const backgroundColor = "white"
export const s = StyleSheet.create({
    createArticleWrapper: {
        paddingLeft: 15,
        paddingRight: 15,
        backgroundColor: "white",
        height: "100%"
    },
    title: {
        marginTop: 10,
        textAlign: "center",
        fontSize: 20,
        paddingBottom: 10,
        borderBottomWidth: 1,
        marginBottom: 15
    },
    adress: {
        padding: 0,
        marginBottom: 15,
        fontSize: 15,
        backgroundColor: "#FBFBFB"

    },
    articleTextContainer: {
        backgroundColor: "#FBFBFB",
        borderWidth: 1,
        borderColor: "lightgray",
        height: 250
    },
    articleText: {
        marginLeft: 10,
        marginRight: 10,
        fontSize: 15
    },
    openingHours: {
        padding: 0,
        marginBottom: 15,
        fontSize: 15,
        backgroundColor: "#FBFBFB"
    },
    selectCategoriesText: {
        fontSize: 15,
        marginRight: 10,
        borderWidth: 1,
        borderColor: "#0056B9",
        paddingLeft: 5,
        paddingRight: 5,
        color: "#0056B9",
        borderRadius: 20,
        marginTop: 5,
        textAlign: "center"
    },
    openedSelectedCategoriesText : {
        fontSize: 15,
        marginRight: 10,
        borderWidth: 1,
        borderColor: "gray",
        paddingLeft: 5,
        paddingRight: 5,
        color: "white",
        borderRadius: 20,
        marginTop: 5,
        textAlign: "center",
        backgroundColor: "gray"
    },

    categories: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "center",
        justifyContent: "center"
    },

    buttonsWrapper: {
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "center",
        flex: 1
    },
    button: {
        backgroundColor: "#0056B9",
        padding: 10,
        width: 120,
        borderRadius: 5,
        margin: 25
    },
    buttonText: {
        color: "white",
        textAlign: "center"
    }
});