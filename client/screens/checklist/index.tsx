import { View, Text, FlatList, StyleSheet } from "react-native"
import { SafeAreaView } from "react-native-safe-area-context"
import { s } from "./styles"
import { useEffect, useState } from "react"
import { IChecklist, ITranslatedChecklist } from "../../types/checklist"
import { apiGetTranslatedChecklist, apiSetUserAtStep } from "../../api"
import { ChecklistItem } from "../../components/touritem"
import { useTranslation } from "react-i18next"
import { useUser } from "../../provider/user"

export const ChecklistScreen = () => {
    const [checklist, setChecklist] = useState<ITranslatedChecklist>()
    const [atStep, setAtStep] = useState<number>(1)
    const { t, i18n } = useTranslation()
    const { user } = useUser()
    const setUserAtStep = async (step: number) => {
        if (user) {
            await apiSetUserAtStep(user.userId, step)
        }
    }
    const handleCheckItem = (index: number) => {
        if (index + 1 === atStep) {
            setAtStep(atStep - 1)
            setUserAtStep(atStep - 1)
        } else {
            setAtStep(index + 1)
            setUserAtStep(index + 1)
        }
    }
    useEffect(() => {
        const getChecklist = async () => {
            const newChecklist = await apiGetTranslatedChecklist("1", i18n.language)
            setChecklist(newChecklist)
        }
        getChecklist()
        if (user) setAtStep(user.checklistAtStep)

    }, [i18n.language, user])
    return (
        <SafeAreaView style={s.container}>
            <View style={s.headerContainer}>
                <Text style={s.headerText}>
                    {t("checklist.header")} {checklist?.location}
                </Text>
            </View>
            {checklist && <FlatList
                data={checklist.translatedSteps}
                renderItem={({ item, index }) => <ChecklistItem {...item} isLastElement={checklist.translatedSteps.length - 1 === index ? true : false} setIsChecked={() => handleCheckItem(index + 1)} isChecked={index + 1 < atStep} />}
                keyExtractor={(item, index) => index.toString()}
                contentContainerStyle={s.listContainer}
                style={s.checklist}
            />}
        </SafeAreaView>
    )
}

