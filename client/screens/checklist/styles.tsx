import { StyleSheet } from "react-native"
const backgroundColor = "white"
export const s = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: backgroundColor
  },
  headerContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    marginTop: 20
  },
  headerText: {
    fontSize: 18,
    fontWeight: '500',
  },
  listContainer: {
  },
  checklist: {
  }
});