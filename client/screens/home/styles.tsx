import { StyleSheet } from "react-native"
const backgroundColor = "#FBFBFB"
export const s = StyleSheet.create({
    container: {
        backgroundColor: backgroundColor,
    },
    title: {
    },
    articles: {
        padding: 0,
        textAlign: "left",
        marginBottom: 50,
    },
    search: {
        backgroundColor: backgroundColor,
        height: 50,
        padding: 0,
        flexDirection: "row",
        alignItems: "center",
        borderColor: "lightgray",
        borderBottomWidth: 1,
    },
    searchText: {
        marginLeft: 20,
        width: "85%"
    },
    addArticleButton: {
        position: "absolute",
        bottom: 70,
        right: 175,
    },
    addArticleIcon: {
        fontSize: 40,
        backgroundColor: "#0056B9",
        borderRadius: 100,
        height: 55,
        width: 55,
        alignItems: "center",
        justifyContent: "center",
        textAlign: "center",
        color: "white",
    },
})