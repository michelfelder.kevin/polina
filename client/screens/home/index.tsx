import { s } from "./styles"
import { FlatList, View, Text, TextInput, TouchableOpacity, RefreshControl } from "react-native"
import { ListArticle } from "../../components/listArticle"
import { useEffect, useState } from "react"
import { apiGetAllArticles, apiGetAllCategories, apiGetAllTranslatedArticles, apiGetAllTranslatedCategories } from "../../api"
import { IArticle, ITranslatedArticle } from "../../types/article"
import { SafeAreaView } from "react-native-safe-area-context"
import { ParamListBase, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"
import { useTranslation } from "react-i18next"
import { useCategories } from "../../provider/category"
import { useUser } from "../../provider/user"
import { calculateUpvotes } from "../../helper"
export const HomeScreen = () => {
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const [searchQuery, setSearchQuery] = useState("")
    const [filteredArticles, setFilteredArticles] = useState<ITranslatedArticle[]>();
    const [articles, setArticles] = useState<ITranslatedArticle[]>()
    const [isRefreshing, setIsRefreshing] = useState(false)
    const { categories } = useCategories()
    const { t, i18n } = useTranslation()
    const { user } = useUser()
    const getAllArticles = async () => {
        const allArticles: ITranslatedArticle[] = await apiGetAllTranslatedArticles(i18n.language)
        const sortedArticles = allArticles.sort((a, b) => calculateUpvotes(a.voters.upvotedBy, a.voters.downvotedBy) > calculateUpvotes(b.voters.upvotedBy, b.voters.downvotedBy) ? -1 : 1)
        setArticles(sortedArticles)
        setFilteredArticles(sortedArticles)
    }
    const onRefresh = () => {
        setIsRefreshing(true)
        getAllArticles()
        setTimeout(() => {
            setIsRefreshing(false)
        }, 200)
    }
    const handleSearch = (query: string) => {
        setSearchQuery(query);
        if (query.trim() === '') {
            setFilteredArticles(articles)
        } else {
            if (categories) {
                const filteredData = articles?.filter((article) =>
                    //Refactor this shit :D
                    article.translatedTitle.toLowerCase().includes(query.toLowerCase()) || categories.find((category) => article.categories.find((categoryId) => categoryId === category.categoryId))?.translatedText.toLowerCase().includes(query.toLowerCase())
                )
                setFilteredArticles(filteredData)
            }
        }
    }
    const handleCancelSearch = () => {
        setSearchQuery("")
        handleSearch("")
    }
    useEffect(() => {
        getAllArticles()
    }, [i18n.language])
    return (
        <SafeAreaView style={s.container}>
            <View style={s.search}>
                <TextInput style={s.searchText} placeholder={t("search")} onChangeText={(text) => handleSearch(text)} value={searchQuery}>
                </TextInput>
                {searchQuery !== "" && <TouchableOpacity onPress={() => handleCancelSearch()}><Text>X</Text></TouchableOpacity>}
            </View>
            <FlatList
                refreshControl={
                    <RefreshControl
                        refreshing={isRefreshing}
                        onRefresh={onRefresh} />
                }
                style={s.articles}
                data={filteredArticles}
                renderItem={({ item }) => <ListArticle article={item}
                    key={item.articleId}
                />
                }
            >
            </FlatList>
        </SafeAreaView>
    )
}
