import { StyleSheet } from "react-native"
const backgroundColor = "white"
export const s = StyleSheet.create({
    articleContainer: {
        backgroundColor: backgroundColor,
    },
    title: {
        fontWeight: "700",
        fontSize: 20,
        textAlign: "left",
        marginTop: 10,
        marginBottom: 10,
        marginLeft: 15
    },
    categories: {
        flexDirection: "row",
        marginLeft: 15
    },
    categoryText: {
        fontSize: 12,
        marginRight: 5,
        borderWidth: 1,
        borderColor: "#0056B9",
        paddingLeft: 5,
        paddingRight: 5,
        color: "#0056B9",
        borderRadius: 20,
    },
    locationDateWrapper: {
        marginTop: 10,
        flexDirection: "row",
        marginLeft: 15
    },
    locationDateText: {
        color: "gray",
        fontSize: 12,
    },
    adressHoursWrapper: {
        flexDirection: "row",
        marginBottom: 15,
        marginLeft: 15,
    },
    articleWrapper: {
    },
    articleTextWrapper: {
        paddingLeft: 15,
        paddingRight: 15,
        borderBottomColor: "lightgray",
        borderBottomWidth: 1,
        paddingBottom: 15
    },
    commentsWrapper: {
        marginLeft: 15,
        marginTop: 15,
    },
    comments: {
    },
    addComment: {
        marginBottom: 20
        
    },
    addCommentInput: {
        backgroundColor: "#FBFBFB",
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5
    },
    addCommentFooter: {
        flexDirection: "row",
        marginLeft: 15
    },
    addCommentUnderneath: {
        alignItems: "center",
        justifyContent: "center",
        height: 70,
        flexDirection: "column",

    },
    addCommentUnderneathText: {
        borderWidth: 1,
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderColor: "#0056B9",
        color: "#0056B9"
    },
    commentFooterText: {
        fontSize: 10,
        color: "gray",
        marginRight: 3
    },
    commentsTitle: {
        fontSize: 20,
        marginBottom: 15
    },
    footerWrapper: {
        flexDirection: "row",
        marginLeft: 15,
        marginTop: 5
    },
    footerText: {
        color: "gray",
        marginLeft: 3,
        fontSize: 12
    },
    author: {
        fontWeight: "500",
        marginTop: 10
    },
    date: {
        marginTop: 10,
        color: "gray"
    }
});