import { RouteProp } from "@react-navigation/native";
import { View, Text, FlatList, TouchableOpacity, ScrollView, TextInput } from "react-native"
import { RootStackParamList } from "../../types/rootStackParams";
import { s } from "./styles"
import { ArticleComment } from "../../components/articleComment";
import { Upvote } from "../../components/upvote";
import { IArticle, IComment, ILanguageTexts, ITranslatedArticle } from "../../types/article";
import { useEffect, useState } from "react";
import { apiAddCommentToArticle, apiGetArticle, apiGetTranslatedArticle, apiReportArticle } from "../../api";
import { useTranslation } from "react-i18next";
import { useCategories } from "../../provider/category";
import { useUser } from "../../provider/user";
import { calculateUpvotes } from "../../helper";
import { ReportModal } from "../../components/reportModal";
type ArticleScreenRouteProp = RouteProp<RootStackParamList, 'Article'>;
type ArticleScreenProps = {
    route: ArticleScreenRouteProp
}
export const ArticleScreen: React.FC<ArticleScreenProps> = ({ route }) => {
    const { articleId } = route.params
    const articleIdString = articleId.toString()
    const [isCommenting, setIsCommenting] = useState(false)
    const [newCommentText, setNewCommentText] = useState("")
    const [article, setArticle] = useState<ITranslatedArticle>()
    const { categories } = useCategories()
    const [date, setDate] = useState("")
    const { t, i18n } = useTranslation()
    const { user } = useUser()
    const [isReportModalOpen, setIsReportModalOpen] = useState(false)
    const handleReportButton = (reason: string) => {
        const reportArticle = async () => {
            if(article) {
                await apiReportArticle(article.articleId, reason)
            }
        }
        reportArticle()
        setIsReportModalOpen(!isReportModalOpen)
    }
    const handleAddCommentButton = async () => {
        if (!user) {
            alert(t("alert.login"))
            return
        }
        if (!article || newCommentText === "") return null
        const languageText: ILanguageTexts = {
            language: i18n.language,
            text: newCommentText
        }
        const updatedArticleComments = await apiAddCommentToArticle(article.articleId, languageText, user.name)
        article.comments = updatedArticleComments
        const updatedArticle = article
        setArticle(updatedArticle)
        setNewCommentText("")
        setIsCommenting(false)
    }
    useEffect(() => {
        const getArticle = async () => {
            const thisArticle: ITranslatedArticle = await apiGetTranslatedArticle(articleIdString, i18n.language)
            setArticle(thisArticle)
        }
        getArticle()

    }, [i18n.language, articleId])
    useEffect(() => {
        if (article) {
            const date = new Date(article.date)
            const day = date.getDate(); // Gets the day as a number (1-31)
            const month = date.getMonth() + 1; // Gets the month as a number (0-11), add 1 to adjust to human-readable format (1-12)
            const year = date.getFullYear(); // Gets the year as a number (e.g., 2023)
            const formattedDate = `${day.toString().padStart(2, '0')}.${month.toString().padStart(2, '0')}.${year}`
            setDate(formattedDate)
        }

    }, [article])
    return (
        <ScrollView>
            <View style={s.articleContainer}>
                <View style={s.articleWrapper}>
                    <ReportModal visible={isReportModalOpen} onClose={() => setIsReportModalOpen(false)} onSubmit={(reason) => handleReportButton(reason)} />

                    <Text style={s.title}>{article?.translatedTitle}</Text>
                    <View style={s.categories}>
                        {article?.categories.map((categoryId, index) => {
                            const foundCategory = categories.find((category) => category.categoryId === categoryId)
                            return (
                                <Text style={s.categoryText} key={index}>{foundCategory?.emoji + " " + foundCategory?.translatedText}</Text>
                            )
                        })}
                    </View>
                    <View style={s.locationDateWrapper}>
                        <Text style={s.locationDateText}>{article?.location}</Text>
                    </View>
                    <View style={s.adressHoursWrapper}>
                        {article?.address ? <Text style={s.locationDateText}>{article.address},</Text> : null}
                        {article?.openingHours ? <Text style={s.locationDateText}> {article.openingHours}</Text> : null}
                    </View>

                    <View style={s.articleTextWrapper}>
                        {article && <Text>{article.translatedText}</Text>}
                        <View style={{ flexDirection: "row" }}>
                            <Text style={s.author}>{article?.author} </Text>
                            <Text style={s.date}>- {date}</Text>
                        </View>
                    </View>
                    <View style={s.footerWrapper}>
                        {article && <Upvote upvotes={calculateUpvotes(article.voters.upvotedBy, article.voters.downvotedBy)} isHorizontal={true} article={article} />}
                        <TouchableOpacity onPress={() => setIsCommenting(!isCommenting)}><Text style={s.footerText}>{t("article.addComment")} |</Text></TouchableOpacity>
                        {/* <TouchableOpacity><Text style={s.footerText}>{t("article.share")} |</Text></TouchableOpacity> */}
                        <TouchableOpacity onPress={() => setIsReportModalOpen(true)}><Text style={s.footerText}>{t("article.report")}</Text></TouchableOpacity>
                    </View>
                </View>
                <View style={s.commentsWrapper}>
                    {article && <Text style={s.commentsTitle}>{article.comments.length} {article.comments.length > 1 ? t("article.comments") : t("article.comment")}</Text>}
                    {article?.comments.map((comment) => (
                        <ArticleComment comment={comment} key={comment.id} />
                    ))}
                </View>
                {isCommenting
                    ?
                    <View style={s.addComment}>
                        <TextInput style={s.addCommentInput} onChangeText={(text) => setNewCommentText(text)} placeholder={t("article.addComment")}>
                        </TextInput>
                        <View style={s.addCommentFooter}>
                            <TouchableOpacity onPress={() => handleAddCommentButton()}>
                                <Text style={s.commentFooterText}>{t("article.save")} |</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => setIsCommenting(false)}>
                                <Text style={s.commentFooterText}>{t("article.cancel")}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    :
                    <View style={s.addCommentUnderneath}>
                        <TouchableOpacity onPress={() => setIsCommenting(!isCommenting)}><Text style={s.addCommentUnderneathText}>{t("article.addComment")}</Text></TouchableOpacity>
                    </View>
                }
            </View>
        </ScrollView>
    )
}