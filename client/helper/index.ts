import { ILanguageTexts } from "../types/article";
import { ICategory } from "../types/category";

export const findLanguageText = (languageTexts: ILanguageTexts[], languageCode: string): ILanguageTexts | undefined => {
    return languageTexts.find(langText => langText.language === languageCode)
}
export const calculateUpvotes = (upvoters: string[], downvoters: string []) => {
    return upvoters.length - downvoters.length
}