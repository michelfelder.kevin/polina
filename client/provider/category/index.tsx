import { createContext, useContext, useEffect, useState } from "react"
import { ICategory, ITranslatedCategory } from "../../types/category"
import { apiGetAllTranslatedCategories } from "../../api"
import { useTranslation } from "react-i18next"
interface ICategoryProviderProps {
    children: React.ReactNode
}
interface ICategoryContext {
    categories: ITranslatedCategory[]
}
const CategoryContext = createContext<ICategoryContext>({ categories: [] })
export const CategoryProvider: React.FC<ICategoryProviderProps> = ({ children }) => {
    const { i18n } = useTranslation()
    const [categories, setCategories] = useState<ITranslatedCategory[]>([])
    useEffect(() => {
        const getAllCategories = async () => {
            const allCategories: ITranslatedCategory[] = await apiGetAllTranslatedCategories(i18n.language);
            setCategories(allCategories);
        };
        getAllCategories();
    }, [i18n.language])
    return (
        <CategoryContext.Provider value={{ categories }}>
            {children}
        </CategoryContext.Provider>
    )
}
export const useCategories = () => {
    const context = useContext(CategoryContext)
    if(!context) {
        throw new Error ("useCategories must be used within a CategoryProvider")
    }
    return context
}