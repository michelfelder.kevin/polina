import { Dispatch, SetStateAction, createContext, useContext, useEffect, useState } from "react"
import * as SecureStore from 'expo-secure-store';
import { apiGetUserByJWT } from "../../api";
interface IUserProviderProps {
    children: React.ReactNode
}
interface IUserContext {
    user: IUser | undefined
    setUser: Dispatch<SetStateAction<IUser | undefined>>
}
const UserContext = createContext<IUserContext | undefined>(undefined)
export const UserProvider: React.FC<IUserProviderProps> = ({ children }) => {
    const [user, setUser] = useState<IUser | undefined>()
    useEffect(() => {
        const getTokenAndUser = async () => {
            const token = await SecureStore.getItemAsync("JWT")
            if (!token) {
                return 
            }
            const user = await apiGetUserByJWT(token)
            setUser(user)
        }
        getTokenAndUser()
    },[])
    return (
        <UserContext.Provider value={{ user, setUser }}>
            {children}
        </UserContext.Provider>
    )
}
export const useUser = () => {
    const context = useContext(UserContext)
    if (!context) {
        throw new Error("useUser must be used within a UserProvider")
    }
    return context
}