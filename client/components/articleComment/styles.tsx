import { StyleSheet } from "react-native"
const backgroundColor = "white"
export const s = StyleSheet.create({
    commentWrapper: {
        flexDirection: "row",
        backgroundColor: "#FBFBFB",
        marginRight: 15,
        borderColor: "lightgray",
        borderWidth: 1,

    },
    contentWrapper: {
        marginLeft: 10,
        flex: 10
    },
    titleWrapper: {
        marginBottom: 5,
        flex: 1,
        flexDirection: "row"
    },
    date: {
        color: "gray"
    },
    userName: {
        fontWeight: "700",
    },
    comment: {
        flex: 1,
        marginBottom: 5
    },
    upvoteWrapper: {
        flex: 1,
        alignItems: "flex-end",
        justifyContent: "center"
    },
    commentFooter: {
        flexDirection: "row",

    },
    footerText: {
        color: "gray",
        marginRight: 3,
        fontSize: 10,
        marginBottom: 20
    }
})