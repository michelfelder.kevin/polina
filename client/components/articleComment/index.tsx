import { View, Text, TouchableOpacity } from "react-native"
import { IComment } from "../../types/article"
import { Upvote } from "../upvote"
import { s } from "./styles"
import { useTranslation } from "react-i18next"
import { calculateUpvotes, findLanguageText } from "../../helper"
import { useState } from "react"
import { ReportModal } from "../reportModal"
import { apiReportComment } from "../../api"
interface IArticleCommentProps {
    comment: IComment
}
export const ArticleComment = ({ comment }: IArticleCommentProps) => {
    const date = new Date(comment.date)
    const day = date.getDate(); // Gets the day as a number (1-31)
    const month = date.getMonth() + 1; // Gets the month as a number (0-11), add 1 to adjust to human-readable format (1-12)
    const year = date.getFullYear(); // Gets the year as a number (e.g., 2023)
    const formattedDate = `${day.toString().padStart(2, '0')}.${month.toString().padStart(2, '0')}.${year}`
    const { t, i18n } = useTranslation()
    const [isReportModalOpen, setIsReportModalOpen] = useState(false)
    const handleReportButton = (reason: string) => {
        const reportComment = async () => {
            await apiReportComment(comment.id, reason)
        }
        reportComment()
        setIsReportModalOpen(!isReportModalOpen)
    }
    return (
        <View>
            <View style={s.commentWrapper}>
                <View style={s.contentWrapper}>
                    <View style={s.titleWrapper}>
                        <Text style={s.userName}>{comment.username}</Text>
                        <Text style={s.date}> - {formattedDate}</Text>
                    </View>
                    <Text style={s.comment}>{findLanguageText(comment.languageTexts, i18n.language)?.text}</Text>
                </View>
                <View style={s.upvoteWrapper}>
                    <Upvote upvotes={calculateUpvotes(comment.voters.upvotedBy, comment.voters.downvotedBy)} isHorizontal={false} comment={comment} />
                </View>
            </View>
            <ReportModal visible={isReportModalOpen} onClose={() => setIsReportModalOpen(false)} onSubmit={(reason) => handleReportButton(reason)} />
            <View style={s.commentFooter}>
                {/* <TouchableOpacity><Text style={s.footerText}>{t("article.reply")} |</Text></TouchableOpacity> */}
                {/* <TouchableOpacity><Text style={s.footerText}>{t("article.share")} |</Text></TouchableOpacity> */}
                <TouchableOpacity onPress={() => setIsReportModalOpen(true)}><Text style={s.footerText}>{t("article.report")}</Text></TouchableOpacity>
            </View>
        </View>
    )
}
