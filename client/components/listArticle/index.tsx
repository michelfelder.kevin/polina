import { View, Text, TouchableOpacity, Image } from "react-native"
import { IArticle, ITranslatedArticle } from "../../types/article"
import { s } from "./styles"
import { ParamListBase, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"
import { Upvote } from "../upvote"
import { useEffect, useState } from "react"
import { ITranslatedCategory } from "../../types/category"
import { useCategories } from "../../provider/category"
import { calculateUpvotes } from "../../helper"
interface IArticleProps {
    article: ITranslatedArticle
}
export const ListArticle = ({ article }: IArticleProps) => {
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const date = new Date(article.date)
    const day = date.getDate(); // Gets the day as a number (1-31)
    const month = date.getMonth() + 1; // Gets the month as a number (0-11), add 1 to adjust to human-readable format (1-12)
    const year = date.getFullYear(); // Gets the year as a number (e.g., 2023)
    const formattedDate = `${day.toString().padStart(2, '0')}.${month.toString().padStart(2, '0')}.${year}`
    const { categories } = useCategories()
    return (
        <View style={s.article} >
            <Upvote upvotes={calculateUpvotes(article.voters.upvotedBy, article.voters.downvotedBy)} isHorizontal={false} article={article} />
            <TouchableOpacity onPress={() => navigation.navigate("Article", { articleId: article.articleId })} style={s.articleContentWrapper}>
                <View style={s.title}>
                    <Text style={s.titleText} numberOfLines={2} >{article.translatedTitle}</Text>
                </View>
                <View style={s.locationDateWrapper}>
                    <Text style={s.locationDateText}>{article.location},</Text>
                    <Text style={s.locationDateText}> {formattedDate}</Text>
                </View>
                <View style={s.adressHoursWrapper}>
                    {article.address ? <Text style={s.locationDateText}>{article.address},</Text> : null}
                    {article.openingHours ? <Text style={s.locationDateText}> {article.openingHours}</Text> : null}
                </View>
                <View style={s.categories}>
                    {article.categories.map((categoryId, index) => {
                        const foundCategory = categories.find((category) => category.categoryId === categoryId)
                        return (
                            <Text style={s.categoryText} key={index}>{foundCategory?.emoji + " " + foundCategory?.translatedText}</Text>
                        )
                    })}
                </View>
            </TouchableOpacity>

        </View>
    )
}