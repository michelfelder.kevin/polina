import { StyleSheet } from "react-native"
const backgroundColor = "white"
export const s = StyleSheet.create({
    article: {
        flexDirection:"row",
        paddingTop: 20,
        paddingBottom: 20,
        borderColor: "lightgray",
        borderBottomWidth: 1,
        backgroundColor: backgroundColor,
    },
    voteImage: {
        height: 20,
        width: 20,
        
    },
    upvotes: {
        width: 40,
        alignItems: "center",
        justifyContent: "center",
        
    },
    title: {
        width: 400,
     },
    titleText: {
        fontWeight: "bold",
        textAlign: "left",
        includeFontPadding: false,
        textAlignVertical: "center",    
        width: "90%",
        fontSize: 15,
        marginBottom: 2.5,
    },
    articleContentWrapper: {
        paddingLeft: 10

    },
    locationDateWrapper: {
        flexDirection: "row",
    },
    locationDateText: {
        color: "gray",
        fontSize: 12,
    },
    adressHoursWrapper: {
        flexDirection: "row",
        marginBottom: 5

    },
    categories: {
        flexDirection: "row",
    },
    categoryText: {
        fontSize: 12,
        marginRight: 10,
        borderWidth: 1,
        borderColor: "#0056B9",
        paddingLeft: 5,
        paddingRight: 5,
        color: "#0056B9",
        borderRadius: 20,
    },
})