import { StyleSheet } from "react-native"
const backgroundColor = "#0056B9"
export const s = StyleSheet.create({
    navbar: {
        backgroundColor: backgroundColor,
        height: 50,
        margin: 0,
        padding: 0,
        flexDirection: "row",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 20,
        },
        shadowOpacity: 0.1,
        shadowRadius: 2,
    
        // Elevation for Android
        elevation: 4,
    },
    location: {
        flex: 10,
        
    },
    locationText: {
        marginLeft: 20,
        color: "white"
    },
    languageDropdown: {
        backgroundColor: "#FBFBFB",
        maxHeight: 500,
        overflow: "scroll"
    },
    language: {
        flex: 1,
        alignItems: "center",
    },
    languageButton: {
        width: 100,
        backgroundColor: backgroundColor,
        alignItems: "center",
        justifyContent: "center",
    },
    wrapper: {
        flex: 10,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "flex-end"
        
    },
    notifications: {
        marginRight: 15
    },
    profile: {
        marginRight: 20
    },
    profileImage: {
        width: 25,
        height: 25,
        borderRadius: 50,
        borderColor: "violet",
        borderWidth: 1.5
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalView: {
        width: '80%',
        padding: 20,
        backgroundColor: 'white',
        borderRadius: 10,
        alignItems: 'center',
        elevation: 5, // shadow for Android
        shadowColor: '#000', // shadow for iOS
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4
    },
})