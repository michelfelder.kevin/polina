import { View, Text, TouchableOpacity, Modal, Image } from "react-native"
import { s } from "./styles"
import { useState } from "react"
import SelectDropdown from "react-native-select-dropdown"
import { useTranslation } from "react-i18next"
import { ELanguages } from "../../localization/languages"
import { ParamListBase, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"


export const Navbar = () => {
    const [isLocationModalOpen, setIsLocationModalOpen] = useState(false)
    const [isLanguageModalOpen, setIsLanguageModalOpen] = useState(false)
    const [location, setLocation] = useState("Ulm/Neu-Ulm")
    const { i18n } = useTranslation();
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const getFlagByLanguageCode = (code: string): string | undefined => {
        const entries = Object.entries(ELanguages);
        const foundEntry = entries.find(([, value]) => value === code);
        return foundEntry ? foundEntry[0] : undefined;
    }
    const handleLocationButton = () => {
        // setIsLocationModalOpen(!isLocationModalOpen)
    }
    const handleLanguageButton = (selectedLanguage: string) => {
        setIsLanguageModalOpen(!isLanguageModalOpen)
        const selectedLanguageCode = ELanguages[selectedLanguage as keyof typeof ELanguages]
        i18n.changeLanguage(selectedLanguageCode)
    }
    const languages = ["🇩🇪", "🇺🇦", "🇺🇸"]
    return (
        <View style={s.navbar}>
            <View style={s.location}>
                <TouchableOpacity onPress={handleLocationButton}>
                    <Text style={s.locationText} numberOfLines={1}>
                        {location}
                    </Text>
                </TouchableOpacity>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={isLocationModalOpen}
                    onRequestClose={() => setIsLocationModalOpen(false)}>
                    <View style={s.centeredView}>
                        <TouchableOpacity onPress={() => setIsLocationModalOpen(false)} style={s.modalView}>
                            <Text>Coming Soon</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
            <View style={s.language}>
                <SelectDropdown data={languages} onSelect={(value) => handleLanguageButton(value)} buttonStyle={s.languageButton} dropdownStyle={s.languageDropdown} defaultValue={getFlagByLanguageCode(i18n.language)} />

            </View>
            <View style={s.wrapper}>
                <TouchableOpacity style={s.notifications}>
                    <Text>🔔</Text>
                </TouchableOpacity>
                <TouchableOpacity style={s.profile} onPress={() => navigation.navigate("Profile")}>
                    <Image source={require("../../assets/polina.jpeg")} style={s.profileImage} />
                </TouchableOpacity>
            </View>
        </View>
    )
}