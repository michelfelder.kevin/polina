import { View, TouchableOpacity, Image, Text } from "react-native"
import { s } from "./styles"
import { useEffect, useState } from "react"
import { apiVoteArticle, apiVoteComment } from "../../api"
import { useUser } from "../../provider/user"
import { IArticle, IComment, ITranslatedArticle } from "../../types/article"
interface IUpvoteProps {
    upvotes: number,
    isHorizontal: boolean,
    article?: ITranslatedArticle,
    comment?: IComment
}
export const Upvote = ({ upvotes, isHorizontal, article, comment }: IUpvoteProps) => {
    const [articleUpvotes, setArticleUpvotes] = useState(upvotes)
    const [isUpvoted, setIsUpvoted] = useState(false)
    const [isDownvoted, setIsDownvoted] = useState(false)
    const { user } = useUser()
    const handleUpvoteButton = async () => {
        if (!user) {
            alert("Please log in to use this function")
            return
        }
        //TODO: Add upvoted indicator to User
        if (article) {
            await apiVoteArticle(article.articleId, true, user.userId)
        }
        else if (comment) {
            await apiVoteComment(comment.id, true, user.userId)
        }
        setArticleUpvotes(upvotes + 1)
        setIsUpvoted(true)
        setIsDownvoted(false)
    }
    const handleDownvoteButton = async () => {
        if (!user) {
            alert("Please log in to use this function")
            return
        }
        if (article) {
            await apiVoteArticle(article.articleId, false, user.userId)
        }
        else if (comment) {
            await apiVoteComment(comment.id, false, user.userId)
        }
        setArticleUpvotes(upvotes - 1)
        setIsUpvoted(false)
        setIsDownvoted(true)
    }
    useEffect(() => {
        if (article && user) {
            if (article.voters.upvotedBy.includes((user.userId))) setIsUpvoted(true)
        }
        if (comment && user) {
            if (comment.voters.upvotedBy.includes((user.userId))) setIsUpvoted(true)
        }
        setArticleUpvotes(upvotes)
    }, [upvotes])
    return (
        isHorizontal
            ?
            <View style={s.horizontalUpvotes}>
                <TouchableOpacity onPress={() => handleUpvoteButton()} disabled={isUpvoted}>
                    <Image source={require("../../assets/upvote.png")} style={isDownvoted ? s.horizontalGrayedOut : s.horizontalVoteImage} resizeMode="contain" ></Image>
                </TouchableOpacity>
                <Text style={s.horizontalText}>
                    {articleUpvotes}
                </Text>
                <TouchableOpacity onPress={() => handleDownvoteButton()} disabled={isDownvoted}>
                    <Image source={require("../../assets/downvote.png")} style={isUpvoted ? s.horizontalGrayedOut : s.horizontalVoteImage} resizeMode="contain"></Image>
                </TouchableOpacity>
            </View >
            :
            <View style={s.upvotes}>
                <TouchableOpacity onPress={() => handleUpvoteButton()} disabled={isUpvoted}>
                    <Image source={require("../../assets/upvote.png")} style={isDownvoted ? s.grayedOut : s.voteImage} resizeMode="contain" ></Image>
                </TouchableOpacity>
                <Text>
                    {articleUpvotes}
                </Text>
                <TouchableOpacity style={s.voteImage} onPress={() => handleDownvoteButton()} disabled={isDownvoted}>
                    <Image source={require("../../assets/downvote.png")} style={isUpvoted ? s.grayedOut : s.voteImage} resizeMode="contain"></Image>
                </TouchableOpacity>
            </View >
    )
}