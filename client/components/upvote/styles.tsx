import { StyleSheet } from "react-native"
const backgroundColor = "white"
export const s = StyleSheet.create({
    voteImage: {
        height: 20,
        width: 20,
        
    },
    upvotes: {
        width: 40,
        alignItems: "center",
        justifyContent: "center",
        paddingLeft: 10
    },
    grayedOut: {
        height: 20,
        width: 20,
        opacity: 0.5
    },
    horizontalUpvotes: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        marginRight: 5,
        backgroundColor: "#FBFBFB",
        borderWidth: 1, 
        borderColor: "lightgray",
        paddingLeft: 5,
        paddingRight: 5,
        borderRadius: 20


    },
    horizontalVoteImage: {
        height: 12,
        width: 12,
        alignItems: "center",
        justifyContent: "center",
        },
    horizontalGrayedOut: {
        height: 12,
        width: 12,
        opacity: 0.5
    },
    horizontalText: {
        fontSize: 12,
        marginLeft: 3,
        marginRight: 3,
    }
})