import { TouchableOpacity, Text } from "react-native"
import { s } from "./styles"
import { ITranslatedCategory } from "../../types/category"
import { useState } from "react"
interface ISelectableCategoryProps {
    category: ITranslatedCategory,
    onToggle: (category: ITranslatedCategory) => void
    
}

export const SelectableCategory = ({ category, onToggle }: ISelectableCategoryProps) => {
    const [isSelected, setIsSelected] = useState(false)
    const handleToggle = () => {
        setIsSelected(!isSelected)
        onToggle(category)
    }
    return (
        <TouchableOpacity onPress={() => handleToggle()}>
            <Text style={isSelected ? s.selectedCategoryText : s.categoryText} >{category.emoji + " " + category.translatedText}</Text>
        </TouchableOpacity>
    )
}