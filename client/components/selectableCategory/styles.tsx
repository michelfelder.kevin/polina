import { StyleSheet } from "react-native"
const backgroundColor = "#0056B9"
export const s = StyleSheet.create({

    categoryText: {
        fontSize: 15,
        marginRight: 10,
        borderWidth: 1,
        borderColor: "#0056B9",
        paddingLeft: 5,
        paddingRight: 5,
        color: "#0056B9",
        borderRadius: 20,
        marginTop: 5,
    },
    selectedCategoryText: {
        fontSize: 15,
        marginRight: 10,
        borderWidth: 1,
        borderColor: "green",
        paddingLeft: 5,
        paddingRight: 5,
        color: "white",
        borderRadius: 20,
        marginTop: 5,
        backgroundColor: "green"
    }
})