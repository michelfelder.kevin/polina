import { StyleSheet } from "react-native";
export const s = StyleSheet.create({
    listItem: {
        flexDirection: 'row',
        paddingLeft: 24,
        alignItems: 'center',
        marginBottom: 35
    },
    iconWrapper: {
        width: 30,
        height: 30,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 16,
    },
    icon: {
        textAlign: "center"
    },
    checkline: {
        position: "absolute",
        paddingTop: 70,
        fontSize: 25,
        color: "lightgray"
    },
    checkedLine: {
        color: "#FFD800"
    },
    uncheckedIcon: {
        backgroundColor: 'gray'
    },
    checkedIcon: {
        backgroundColor: "#0056B9"
    },
    checkedText: {
        textDecorationColor: "black",
        textDecorationLine: "line-through",
    },
    textWrapper: {
        flex: 1
    },
    titleText: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    subTitleText: {
        fontSize: 14,
        color: 'gray',
    },
})