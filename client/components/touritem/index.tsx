import React, { SetStateAction, useState } from "react";
import { TouchableOpacity, View, Text } from "react-native";
import { s } from "./styles"
import { ParamListBase, useNavigation } from "@react-navigation/native";
import { NativeStackNavigationProp } from "@react-navigation/native-stack";
import { ITranslatedChecklistStep } from "../../types/checklist";
import { useTranslation } from "react-i18next"
interface IChecklistItemProps extends ITranslatedChecklistStep {
    isLastElement: boolean,
    isChecked: boolean,
    setIsChecked: React.Dispatch<SetStateAction<boolean>>
}
export const ChecklistItem = ({
    translatedTitle,
    translatedSubTitle,
    number,
    icon,
    isLastElement,
    articleId,
    isChecked,
    setIsChecked
}: IChecklistItemProps) => {
    const {t} = useTranslation()
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const handlePressTourItem = () => {
        if (!articleId) {
            alert(t("checklist.noArticle"))
            return
        }
        navigation.navigate("Article", { articleId: articleId})
    }
    return (
        <View style={s.listItem}>
            <TouchableOpacity
                onPress={() => setIsChecked(!isChecked)}
                style={[
                    s.iconWrapper,

                    isChecked ? s.checkedIcon : s.uncheckedIcon
                ]}
            >
                <Text style={s.icon}>{icon}</Text>
                {!isLastElement && <Text style={[s.checkline, isChecked ? s.checkedLine : null]}>|</Text>}
            </TouchableOpacity>
            <TouchableOpacity style={[s.textWrapper]} onPress={handlePressTourItem}>
                <Text style={[s.titleText, isChecked ? s.checkedText : null]}>{number}. {translatedTitle}</Text>
                {translatedSubTitle && <Text style={[s.subTitleText, isChecked ? s.checkedText : null]}>{translatedSubTitle}</Text>}
            </TouchableOpacity>
        </View>
    )
}
