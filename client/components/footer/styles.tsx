import { StyleSheet } from "react-native"
const backgroundColor = "#0056B9"
export const s = StyleSheet.create({
    footer: {
        flexDirection: "row",
        height: 60,
        backgroundColor: backgroundColor,
        alignItems: "center"
    },
    item: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        borderColor: "lightgray",
    },
    activeFooter : {
        fontWeight: "700",
        color: "white"
    },
    footerText: {
        color: "white"
    },
    addArticleButton: {
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 100,
        backgroundColor: "white",
        height: 45,
        width: 45,
    },
    addArticle: {
        fontSize: 40,
        textAlign: "center",
        color: "#0056B9",
        padding: 0,
        height: 57
    }
})