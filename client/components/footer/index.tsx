import { TouchableOpacity, View, Text } from "react-native"
import { s } from "./styles"
import { ParamListBase, useNavigation } from "@react-navigation/native"
import { NativeStackNavigationProp } from "@react-navigation/native-stack"
import { useState } from "react"
import { useTranslation } from "react-i18next"
import { useUser } from "../../provider/user"

export const Footer = () => {
    const navigation = useNavigation<NativeStackNavigationProp<ParamListBase>>()
    const [isHomePage, setIsHomePage] = useState<boolean>(true)
    const { t } = useTranslation()
    const { user } = useUser()
    const handleCreateArticleButton = () => {
        if (!user) {
            alert("Please sign in to use this function")
            return
        }
        navigation.navigate("CreateArticle")
    }
    const handleNavigation = (target: string) => {
        if (target === "Home") {
            setIsHomePage(true)
        } else {
            setIsHomePage(false)
        }
        navigation.navigate(target)

    }
    return (
        <View style={s.footer}>
            <TouchableOpacity style={[s.item]} onPress={() => handleNavigation("Home")}>
                <Text style={isHomePage ? s.activeFooter : s.footerText}>{t("footer.home")}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={s.addArticleButton} onPress={handleCreateArticleButton}>
                <Text style={s.addArticle}>+</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[s.item]} onPress={() => handleNavigation("Checklist")}>
                <Text style={isHomePage ? s.footerText : s.activeFooter}>{t("footer.checklist")}</Text>
            </TouchableOpacity>
        </View>
    )
}