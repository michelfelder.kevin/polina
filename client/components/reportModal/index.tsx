import { useMemo, useState } from "react"
import { Modal, View, TouchableOpacity, Button, Text, TouchableWithoutFeedback } from "react-native"
import { s } from "./styles"
import { RadioGroup } from "react-native-radio-buttons-group"
import { useTranslation } from "react-i18next"
interface IReportModalProps {
    visible: boolean,
    onClose: () => void,
    onSubmit: (reason: string) => void
}
export const ReportModal = ({ visible, onClose, onSubmit }: IReportModalProps) => {
    const { t, i18n } = useTranslation()
    const [selectedId, setSelectedId] = useState<string>("1")
    const handleSubmit = () => {
        const reason = radioButtons.find((button) => button.id === selectedId)?.value
        if (reason) {
            onSubmit(reason)
        }
        onClose()
    }
    const radioButtons = useMemo(() => ([
        {
            id: '1',
            label: t("report.translation"),
            value: 'translation'
        },
        {
            id: '2',
            label: t("report.inappropriate"),
            value: 'inappropriate'
        },
        {
            id: '3',
            label: t("report.wrongInformation"),
            value: 'wrongInformation'
        }
    ]), [i18n.language])
    return (
        <Modal visible={visible} transparent onRequestClose={onClose}>
            <TouchableWithoutFeedback onPress={onClose}>
                <View style={s.modalOverlay}>
                    <View style={s.modalView}>
                        <View style={s.modalContent}>
                            <Text style={s.header}>{t("report.selectReason")}</Text>
                            <RadioGroup
                                radioButtons={radioButtons}
                                onPress={(id) => setSelectedId(id)}
                                selectedId={selectedId}
                                containerStyle={s.radioContainer}
                            />
                            <View style={s.buttonsGroup}>
                                <TouchableOpacity style={s.buttonCancel} onPress={() => onClose()}>
                                    <Text style={s.buttonText}>
                                        {t("report.cancel")}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={s.buttonSubmit} onPress={() => handleSubmit()}>
                                    <Text style={s.buttonText}>
                                    {t("report.submit")}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    )
}