import { StyleSheet } from "react-native"
const backgroundColor = "white"
export const s = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalOverlay: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)', // Semi-transparent background
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    modalContent: {
        display: "flex",
        alignItems: "center",
    },
    radioContainer: {
        display: "flex",
        alignItems: "flex-start"
    },
    buttonsGroup: {
        display: "flex",
        flexDirection: "row",
        marginTop: 32
    },
    header: {
        fontSize: 20,
        marginBottom: 16
    },
    buttonCancel: {
        backgroundColor: "gray",
        padding: 8,
        paddingHorizontal: 24,
        marginRight: 16
    },
    buttonSubmit: {
        backgroundColor: "#0056B9",
        padding: 8,
        marginLeft: 16,
        paddingHorizontal: 24
    },
    buttonText: {
        color: "white"
    }
})