import { Navbar } from './components/navbar';
import { SafeAreaView } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { HomeScreen } from './screens/home';
import { ChecklistScreen } from './screens/checklist';
import { Footer } from './components/footer';
import { ArticleScreen } from './screens/article';
import { RootStackParamList } from './types/rootStackParams';
import { CreateArticle } from './screens/createArticle';
import "./localization/i18n"
import { CategoryProvider } from './provider/category'
import { ProfileScreen } from './screens/profile'
import { UserProvider } from './provider/user'
import { RegisterScreen } from './screens/register';
import { LoginScreen } from './screens/login';
const Stack = createNativeStackNavigator<RootStackParamList>();
export default function App() {
  return (
    <UserProvider>
      <CategoryProvider>
        <NavigationContainer>
          <SafeAreaView>
            <Navbar />
          </SafeAreaView>
          <Stack.Navigator>
            <Stack.Screen name="Register" component={RegisterScreen} options={{ headerShown: false, animation: "slide_from_left" }} />
            <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false, animation: "slide_from_right" }} />
            <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
            <Stack.Screen name="Checklist" component={ChecklistScreen} options={{ headerShown: false, animation: "slide_from_right" }} />
            <Stack.Screen name="Article" component={ArticleScreen} options={{ headerShown: false, animation: "slide_from_bottom" }} />
            <Stack.Screen name="CreateArticle" component={CreateArticle} options={{ headerShown: false, animation: "slide_from_left" }} />
            <Stack.Screen name="Profile" component={ProfileScreen} options={{ headerShown: false, animation: "slide_from_right" }} />
          </Stack.Navigator>
          <Footer />
        </NavigationContainer>
      </CategoryProvider>
    </UserProvider>
  );
}